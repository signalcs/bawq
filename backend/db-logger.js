
var redis       = require('redis'),
	config 		= require('config'),
	Mongoose 	= require('mongoose'),
	events      = require('./events'),
	moment		= require('moment'),
	db_mongoose	= require('./model/mongoose');

var redis_auth 	= config.get('redis-auth');
var pubSub 		= redis.createClient();

var gps_log 	= Mongoose.model('gps_log');

if(redis_auth){
	pubSub.auth(redis_auth);
}

pubSub.psubscribe('device:data:*');
pubSub.psubscribe('device:blindarea:*');

pubSub.on('pmessage', function(pattern, channel, message){

	message = JSON.parse(message);
	message.time = moment(message.time, 'YYYY-MM-DD HH:mm:ssZ').toDate();

	var location = {
    	systemno 	: message.systemno,
		time 		: message.time,
		location 	: message.location,
		speed 		: message.speed,
		orientation : message.orientation,
		locate	 	: message.locate,
		digitalout 	: message.digitalout,
		accstatus  	: message.accstatus,
		devicestatus: message.devicestatus,
		oil	 		: message.oil,
		voltage	 	: message.voltage,
		mileage	 	: message.mileage,
		temperature : message.temperature
    };

	events.emit('mongo:insert', location);
	    
});

events.on('mongo:insert', function(location){
	var gps = gps_log(location);
	// insert gps logs data
	gps.save(function(err, gps){
		if(err) console.log(err);
		console.log("Location saved");
	});
});

