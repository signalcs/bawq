
exports.seed = function(knex, Promise) {
  return Promise.join(
    // Deletes ALL existing entries
    knex('drivers').del(), 

    // Inserts seed entries
    knex('drivers').insert({id: 1, name: 'Junaid Abbas', phone: '36988102', email: 'junaid.abbas@signalcs.com', address: 'Bahrain', company_id: 1}),
    knex('drivers').insert({id: 2, name: 'Waleed Mohammed', phone: '36988107', email: 'waleed.mohammed@signalcs.com', address: 'Bahrain', company_id: 1}),
    knex('drivers').insert({id: 3, name: 'Ali Helal', phone: '38988103', email: 'ali.helal@signalcs.com', address: 'Bahrain', company_id: 1}),
    knex('drivers').insert({id: 4, name: 'Noman Chowdhury', phone: '36988105', email: 'noman.chowdhury@signalcs.com', address: 'Bahrain', company_id: 1}),
    knex('drivers').insert({id: 5, name: 'Abdulla Albassri', phone: '38988108', email: 'abdulla.albassri@signalcs.com', address: 'Bahrain', company_id: 1}),
    knex('drivers').insert({id: 6, name: 'Mohammed Ibrahim', phone: '36988109', email: 'mohammed.ibrahim@signalcs.com', address: 'Bahrain', company_id: 1}),
    knex('drivers').insert({id: 7, name: 'Shajeeb Puthan', phone: '36988043', email: 'shajeeb.puthan@signalcs.com', address: 'Bahrain', company_id: 1}),
    knex('drivers').insert({id: 8, name: 'Rusty Rojo', phone: '36988031', email: 'rusty.rojo@signalcs.com', address: 'Bahrain', company_id: 1})
  );
};
