
exports.seed = function(knex, Promise) {
  	return Promise.join(
	    // Deletes ALL existing entries
	    knex('vehicles').del(), 

	    knex('vehicles').insert({id: 1, systemno: '13880000114', plateno: 'Test Vehicle 1', simcard:null, timezone: '+0300', device_type: 'M528', object_type: 'truck', fleet_id: 1,company_id:1,status:'approved' }),
	    knex('vehicles').insert({id: 2, systemno: '861074021454338', plateno: 'Test Vehicle 2', simcard:null, timezone: '+0300', device_type: 'GV300', object_type: 'truck', fleet_id: 1,company_id:1,status:'approved' }),
	    knex('vehicles').insert({id: 3, systemno: '13880000017', plateno: '440921', simcard:null, timezone: '+0300', device_type: 'M528', object_type: 'car', fleet_id: 1,company_id:1,status:'approved' }),
  		knex('vehicles').insert({id: 4, systemno: '13880000231', plateno: '386093', simcard:null, timezone: '+0300', device_type: 'M528', object_type: 'car', fleet_id: 1,company_id:1,status:'approved' }),
	    knex('vehicles').insert({id: 5, systemno: '13889999224', plateno: '386097', simcard:null, timezone: '+0300', device_type: 'M528', object_type: 'car', fleet_id: 1,company_id:1,status:'approved' }),
	    knex('vehicles').insert({id: 6, systemno: '13889999001', plateno: '456542', simcard:null, timezone: '+0300', device_type: 'M528', object_type: 'car', fleet_id: 1,company_id:1,status:'approved' }),
	    knex('vehicles').insert({id: 7, systemno: '13880000003', plateno: '456549', simcard:null, timezone: '+0300', device_type: 'M528', object_type: 'car', fleet_id: 1,company_id:1,status:'approved' }),
	    knex('vehicles').insert({id: 8, systemno: '13880000001', plateno: '24904', simcard:null, timezone: '+0300', device_type: 'M528', object_type: 'car', fleet_id: 1,company_id:1,status:'approved' }),
	    knex('vehicles').insert({id: 9, systemno: '13880000010', plateno: '337375', simcard:null, timezone: '+0300', device_type: 'M528', object_type: 'car', fleet_id: 1,company_id:1,status:'approved' })
  	);
};
