
exports.seed = function(knex, Promise) {
  	return Promise.join(
	    // Deletes ALL existing entries
	    knex('fleets').del(), 

	    // Inserts seed entries
	    knex('fleets').insert({id: 1, name: 'SignalCS Fleet1', company_id: 1 }),
	    knex('fleets').insert({id: 2, name: 'SignalCS Fleet2', company_id: 1 }),
	    knex('fleets').insert({id: 3, name: 'Egypt Fleet1', company_id: 2 }),
	    knex('fleets').insert({id: 4, name: 'Egypt Fleet2', company_id: 2 })
  	);
};
