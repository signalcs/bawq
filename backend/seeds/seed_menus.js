
exports.seed = function(knex, Promise) {
  	return Promise.join(
	    // Deletes ALL existing entries
	    knex('menus').del(), 
	    // Inserts seed entries
	    knex('menus').insert({id: 1, name: 'Dashboard', icon: 'fa-tachometer', dest:'/admin/dashboard', parent_id: 0, module: 'admin' }),
	    knex('menus').insert({id: 2, name: 'Organizations', icon: 'fa-building', dest:'/admin/organization', parent_id: 0, module: 'admin' }),
	    knex('menus').insert({id: 3, name: 'Groups', icon: 'fa-truck', dest:'/admin/groups', parent_id: 0, module: 'admin' }),
	    knex('menus').insert({id: 4, name: 'Devices', icon: 'fa-car', dest:'/admin/devices', parent_id: 0, module: 'admin' }),
		knex('menus').insert({id: 5, name: 'Users', icon: 'fa-users', dest:'/admin/users', parent_id: 0, module: 'admin' }),
		knex('menus').insert({id: 6, name: 'Drivers', icon: ' fa-user ', dest:'/admin/drivers', parent_id: 0, module: 'admin' }),
		knex('menus').insert({id: 7, name: 'SimCards', icon: 'fa-mobile ', dest:'/admin/simcard', parent_id: 0, module: 'admin' })
  	);
};
