
exports.seed = function(knex, Promise) {
  	return Promise.join(
	    // Deletes ALL existing entries
	    knex('users_fleets').del(), 
	    // Inserts seed entries
	    knex('users_fleets').insert({user_id: 1, fleet_id: 1,company_id:1})
  	);
};
