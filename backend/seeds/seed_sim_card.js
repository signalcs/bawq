
exports.seed = function(knex, Promise) {
  	return Promise.join(
	    // Deletes ALL existing entries
	    knex('sim_cards').del(), 
	    // Inserts seed entries
	    knex('sim_cards').insert({id:1,simcard_number: '97336988031', type: 'standard',status:'unused'}),
	    knex('sim_cards').insert({id:2,simcard_number: '97336988103', type: 'standard',status:'unused'}),
	    knex('sim_cards').insert({id:3,simcard_number: '97336988102', type: 'standard',status:'unused'}),
	    knex('sim_cards').insert({id:4,simcard_number: '97336988331', type: 'standard',status:'unused'}),
	    knex('sim_cards').insert({id:5,simcard_number: '97336988403', type: 'standard',status:'unused'}),
	    knex('sim_cards').insert({id:6,simcard_number: '97336988602', type: 'standard',status:'unused'}),
	    knex('sim_cards').insert({id:7,simcard_number: '97336988603', type: 'standard',status:'unused'}),
	    knex('sim_cards').insert({id:8,simcard_number: '97336988604', type: 'standard',status:'unused'}),
	    knex('sim_cards').insert({id:9,simcard_number: '97336988605', type: 'standard',status:'unused'}),
	    knex('sim_cards').insert({id:10,simcard_number: '97336988606', type: 'standard',status:'unused'}),
	    knex('sim_cards').insert({id:11,simcard_number: '97336988607', type: 'standard',status:'unused'})
  	);
};
