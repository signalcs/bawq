
exports.seed = function(knex, Promise) {
  	return Promise.join(
	    // Deletes ALL existing entries
	    knex('services').del(), 
	    // Inserts seed entries
	    knex('services').insert({id:1,service_name: 'SMS receiving and sending', }),
	    knex('services').insert({id:2,service_name: 'Data minimum 100MB', }),
	    knex('services').insert({id:3,service_name: 'HSDPA Function activated', }),
	    knex('services').insert({id:4,service_name: 'Roaming', })
  	);
};
