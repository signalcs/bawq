
exports.seed = function(knex, Promise) {
    return Promise.join(
        // Deletes ALL existing entries
        knex('users_device').del(), 

        // Inserts seed entries
        knex('users_device').insert({user_id: 1, device_id: 1}),
        knex('users_device').insert({user_id: 1, device_id: 2}),
        knex('users_device').insert({user_id: 1, device_id: 3}),
        knex('users_device').insert({user_id: 1, device_id: 4}),
        knex('users_device').insert({user_id: 1, device_id: 5}),
        knex('users_device').insert({user_id: 1, device_id: 6}),
        knex('users_device').insert({user_id: 1, device_id: 7}),
        knex('users_device').insert({user_id: 1, device_id: 8}),
        knex('users_device').insert({user_id: 1, device_id: 9})
    );
};
