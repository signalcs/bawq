
exports.seed = function(knex, Promise) {
  	return Promise.join(
	    // Deletes ALL existing entries
	    knex('companies').del(), 

	    // Inserts seed entries
	    knex('companies').insert({id: 1, name: 'Signal CS', address: 'Juffair, Bahrain', email: 'info@signalcs.com',fax: '17896555',contact_person:'Abdulla Albassri',status:1, tel: '17896555', status: 1}),
	    knex('companies').insert({id: 2, name: 'Signal CS Egypt', address: 'Cairo, Egypt', email: 'info@signalcs.com',fax: '17896555',contact_person:'Abdulla Albassri',status:1, tel: '01093755601', status: 1})
  	);
};
