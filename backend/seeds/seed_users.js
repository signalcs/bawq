
exports.seed = function(knex, Promise) {
  	return Promise.join(
	    // Deletes ALL existing entries
	    knex('users').del(), 

	    // Inserts seed entries
	    knex('users').insert({id: 1, email: 'rusty.rojo@signalcs.com', name: 'Rusty Rojo', username: 'rrojo', password: '$2a$10$LC.ySZahLNLOM/GT3ib85.yUbYQvl7YH9rMp3aHEt1YSWJXlCREM6',company_id:1, role: 'moderator',permissions:'{"com:update": true, "com:engine":true, "com:speed":true, "com:interval": true, "com:timezone": true, "com:power": true}'}),
	    knex('users').insert({id: 2, email: 'company2@moderator.com', name: 'Company2 Moderator', username: 'moderator2', password: '$2a$10$3rOioho4ys3.hCYG64AwFudsqG6rKKWfCr4ZpfWhTYwlI3mhySPe6',company_id:2, role: 'moderator',permissions:''}),
	    knex('users').insert({id: 3, email: 'company23@moderator.com', name: 'Company2 User', username: 'User2', password: '$2a$10$3rOioho4ys3.hCYG64AwFudsqG6rKKWfCr4ZpfWhTYwlI3mhySPe6',company_id:2, role: 'user',permissions:''}),
  		knex('users').insert({id: 4, email: 'admin@signalcs.com', name: 'Administrator', username: 'admin', password: '$2a$10$LC.ySZahLNLOM/GT3ib85.yUbYQvl7YH9rMp3aHEt1YSWJXlCREM6',company_id:1, role: 'admin',permissions:''})
  	);
};
