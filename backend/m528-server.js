
var gps         = require("gps-tracking"),
    config      = require("config"),
    _           = require('lodash'),
    moment      = require('moment'),
    redis       = require('redis');

/* Configuration variables */
var device_opts = config.get('gator_m528');
var redis_auth = config.get('redis-auth');

redisClient = redis.createClient();
redisPub = redis.createClient();
redisSub = redis.createClient();

if(redis_auth){
    redisClient.auth(redis_auth);
    redisPub.auth(redis_auth);
    redisSub.auth(redis_auth);
}

/**
 *
 * Main GPS
 * Devices listener
 *
 */
redisClient.smembers('deviceClients', function(err, reply){
    if(err) console.log(err);

    var vehicles = eval('([' + reply.toString() + '])');
    vehicles = _.keyBy(vehicles, 'systemno');

    // listening to commands send by clients
    redisSub.subscribe('at:command:M528');
    redisSub.on('message', function(channel, message){
        message = JSON.parse(message);
        server.emit('at:command', message);
    });

    server = gps.server(device_opts,function(device,connection){        
        // Location event
        device.on("resp:location",function(data){
            if(data.systemno in vehicles && vehicles[data.systemno].status == 'approved'){
                
                if(!data.locate){
                    var device_timezone = (vehicles[data.systemno] != undefined) ? vehicles[data.systemno].timezone : '+0000';
                    data.time = moment(data.time, 'YYYY-MM-DD HH:mm:ssZ').utcOffset(device_timezone).format('YYYY-MM-DD HH:mm:ss');
                    data.time = data.time+device_timezone;
                }else{
                    var device_timezone = (vehicles[data.systemno] != undefined) ? vehicles[data.systemno].timezone : '+0000';
                    data.time = data.time+device_timezone;
                }

                redisPub.publish('device:data:'+data.systemno, JSON.stringify(data));
            }
        });

        device.on("resp:blindarea",function(data){
            if(data.systemno in vehicles && vehicles[data.systemno].status == 'approved'){
                
                if(!data.locate){
                    var device_timezone = (vehicles[data.systemno] != undefined) ? vehicles[data.systemno].timezone : '+0000';
                    data.time = moment(data.time, 'YYYY-MM-DD HH:mm:ssZ').utcOffset(device_timezone).format('YYYY-MM-DD HH:mm:ss');
                    data.time = data.time+device_timezone;
                }else{
                    var device_timezone = (vehicles[data.systemno] != undefined) ? vehicles[data.systemno].timezone : '+0000';
                    data.time = data.time+device_timezone;
                }

                redisPub.publish('device:blindarea:'+data.systemno, JSON.stringify(data));
            }
        });

        // Command response event
        device.on("ack:command", function(data){
            if(data.systemno in vehicles && vehicles[data.systemno].status == 'approved'){
                redisPub.publish('device:ack:'+data.systemno, JSON.stringify(data));
            }
        });
    });
});
