var util         	= require("util"),
	EventEmitter 	= require("events").EventEmitter,
	io  		   	= require('socket.io'),
    socketioJwt 	= require('socketio-jwt'),
    config     		= require('config'),
    _				= require('lodash'),
    Model       	= require('../model/models'),
    Mongoose    	= require('mongoose'),
    gps_log     	= Mongoose.model('gps_log'),
    redis 			= require('redis');

util.inherits(socketIO, EventEmitter);
util.inherits(PubSub, EventEmitter);

function socketIO(app){
	if(!(this instanceof socketIO)) return new socketIO(app);
	EventEmitter.call(this);

	var socket = this;
	var redisAuth = config.get('redis-auth');
	this.redisClient = redis.createClient();
	this.app = app;
	this.pubsub = [];

	if(redisAuth){
		this.redisClient.auth(redisAuth);
	}

	this.init = function(cb){	
		if(typeof(cb)=="function")cb();
		this.emit("init");
		this.setDebug(true);
	};

	this.doLog = function (msg,from){
		//If debug is disabled, return false
		if(this.getDebug() == false)return false;
		//If from parameter is not set, default is server.
		if(typeof(from) == "undefined")from = "SERVER";
		var msg = "#"+from+": "+msg;
		console.log(msg);
	}

	this.setDebug = function(val){
		this.debug = (val === true);
	}
	this.getDebug = function(){
		return this.debug;
	}

	this.setDeviceClients = function(){
		// temporary code for adding to redis server the
		// deviceClients objects to check device settings
		// todo: remove this code and put in access control
		Model.Vehicle.fetchAll().then(function(collection){
	        var json = collection.toJSON();

	        for (var i = 0; i < json.length; i++) {
	        	socket.redisClient.sadd('deviceClients', JSON.stringify({'systemno': json[i].systemno, 'device_type': json[i].device_type, 
	        		'timezone': json[i].timezone, 'status': json[i].status}));
	        };
	    });
	}

	this.init(function(){
		socket.io = new io();
		socket.io.attach(socket.app);

		socket.io.use(socketioJwt.authorize({
		    secret: config.get('jwt-secret'),
		    handshake: true
		}));

		// save device info to redis
		// note: this function is temporary
		// and needs to be migrated
		socket.setDeviceClients();

		socket.io.on('connection', function(connection) {
			var user = connection.decoded_token;

			// create new instance of pubsub class on every new client
		    connection.pubsub = new PubSub(connection, socket);
		    // push to array for further use of the socket connection
		    socket.pubsub.push(connection);
		    // emit pubsub event on new connection
		    connection.pubsub.emit('connection');

		    // send at commands device
	        connection.on('at:command', function(data){
	            // add permissions from jwt token
	            data.permissions = user.permissions;	            
	            // emit command event in PubSub
	            connection.pubsub.emit('command', data);	           
	        });

	        // disconnects/unsubscribe clients to redis pubsub
	        connection.on('disconnect', function(){
	           	// disconnect socket io and redis
	           	connection.pubsub.unsetSocketDevices(user);
	        });
		});
	});
}

function PubSub(connection, socket){
	/* Inherits EventEmitter class */
	EventEmitter.call(this);

	var redisPS = this;
	var redisAuth = config.get('redis-auth');

	this.socket = socket;
	this.connection = connection;
	this.pub = redis.createClient();
	this.sub = redis.createClient();

	// if redis is on production
	if(redisAuth){
		this.pub.auth(redisAuth);
		this.sub.auth(redisAuth);
	}

	init();
	function init(){
	}

	this.on('connection', function(){
		var user = redisPS.connection.decoded_token;

		// query users device
		redisPS.getUsersDevice(user, function(userDevices){
			// query devices connected to this user
			redisPS.getDevice(userDevices, function(devices){
				// subscribe to devices redis pubsub
				redisPS.subscribe(devices);
				// emit last location to client
				redisPS.emitLastLocation(devices, function(docs){
					redisPS.connection.emit('data:location', { data: JSON.stringify(docs) });
				});
				// save device systemno to redis
				redisPS.setSocketDevices(user, devices);
			});
		});
	});

	this.on('command', function(data){
		// publish command on redis pub
		redisPS.pub.publish('at:command:'+data.device_type, JSON.stringify(data));
		redisPS.doLog('Command is issued by this socket: '+JSON.stringify(data), 'websocket');
	});

	this.getUsersDevice = function(user, callback){
		Model.UsersDevice.query('where','user_id', user.id).fetchAll().then(function(userDevices){
			callback(userDevices.toJSON());
		});
	}

	this.getDevice = function(userDevices, callback){
		userDevices = _.map(userDevices, 'device_id');

		Model.Vehicle.where('id', 'in', userDevices).fetchAll().then(function(collection){
			callback(collection.toJSON());
		});
	}

	this.subscribe = function(devices){
		_.forEach(devices, function(value, key){
			// save vehicles to redis memory
			redisPS.sub.subscribe('device:data:'+value.systemno);
			redisPS.sub.subscribe('device:ack:'+value.systemno);
			redisPS.doLog('client subscribe to channel device:data:'+value.systemno);
		});
	}

	this.publish = function(data){
		redisPS.pub.publish('at:command:'+data.device_type, JSON.stringify(data));
	}

	this.emitLastLocation = function(devices, callback){
		devices = _.map(devices, 'systemno');
		gps_log.aggregate(
            { $match : {'systemno': {$in: devices} } },
            { $group : 
                { 
                    _id: '$systemno',
                    systemno: { $last: '$systemno' }, 
                    location: { $last: '$location' }, 
                    time: { $last: '$time' },
                    speed: { $last: '$speed' },
                    orientation: { $last: '$orientation'},
                    locate: { $last: '$locate'},
                    digitalout: { $last: '$digitalout'},
                    accstatus: { $last: '$accstatus'},
                    devicestatus: { $last: '$devicestatus'},
                    oil: { $last: '$oil'},
                    voltage: { $last: '$voltage'},
                    mileage: { $last: '$mileage'},
                    temperature: { $last: '$temperature'}
                }
            },
            { $project : { "_id":0, 
            	"systemno": 1,
            	"location": 1,
            	"time": 1,
            	"speed": 1,
            	"orientation": 1,
            	"locate": 1,
            	"digitalout": 1,
            	"accstatus": 1,
            	"devicestatus": 1,
            	"oil": 1,
            	"voltage": 1,
            	"mileage": 1,
            	"temperature": 1
            }},
            { $sort : {'time': -1} },
            function(err, doc){
                callback(doc);
            }
	    );
	}

	this.setSocketDevices = function(user, device){
		device = _.join(_.map(device, 'systemno'), ':');

		redisPS.socket.redisClient.set('socketDevices:'+user.id, device, function(err){
			if(err) console.log(err);
			redisPS.doLog('List saved to redis server');
		});
	}

	this.unsetSocketDevices = function(user){
		redisPS.socket.redisClient.get('socketDevices:'+user.id, function(err, reply){
       		var deviceArray = reply.split(':');

       		for (var i = 0; i < deviceArray.length; i++) {
       			redisPS.sub.unsubscribe('device:data:'+deviceArray[i]);
       			redisPS.doLog('unsubscribe to '+deviceArray[i] , 'websocket');
       		};
       	});    
	}

	this.doLog = function (msg){
		redisPS.socket.doLog(msg);	
	}

	// redis pubsub message event sending directly to client published messages
    this.sub.on('message', function(channel, message){
    	var published = JSON.parse(message);

    	switch(published.action){
    		case "resp:update":
    		case "resp:location":
    			var msg = _.omit(published, ['action']);

				if(msg.hasOwnProperty('command') && '/#'+msg.client_id == redisPS.connection.id){
					redisPS.connection.emit('resp:location', { data: JSON.stringify(msg) });
				}
				else {
					redisPS.connection.emit('resp:location', { data: JSON.stringify(msg) });
				}
    		break;

    		case "ack:command":
    			if('/#'+msg.client_id == redisPS.connection.id) { 
    				redisPS.connection.emit('ack:command', { data: JSON.stringify(ack) });
    			}
    		break;
    	}
	});

	// just print out error logs
	this.sub.on('err', function(err){
		redisPS.doLog(err, 'redis');
	});
}

exports.socketIO = socketIO;

