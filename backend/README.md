# Running Migrations

For migrations we need knex.js.

Install knex.js globally

```
#!cmd
npm install knex -g
```

cd on to the api directory

```
#!cmd
cd api-server
```

To edit config files for the database access go to 
config directory and the config file is db_config.js

To run latest migration files just run


```
#!cmd
knex migrate:latest
```

If you want to create new migration files

```
#!cmd
knex migrate:make migration_name
```

If you want to isssue a rollback on the migration files run

```
#!cmd
knex migrate:rollback
```


# RUNNING API

To run api cd on the api-server directory and run


```
#!cmd
node api-server.js
```


```
#!cmd
knex seed:run
```

#TODOS

We need to create a seeder to make sample datas on the mysql database.
I have created sample already please refer to that on seeds folder