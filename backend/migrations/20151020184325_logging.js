
exports.up = function(knex, Promise) {
		  return knex.schema.
  			createTable('logging',function(t){
  				t.increments('id').notNullable().primary();
	      		t.integer('user_id').notNullable().unsigned().references('users.id');
	      		t.integer('company_id').notNullable().unsigned().references('companies.id');
	      		t.integer('model_id');
	      		t.string('model_name',100);
	      		t.string('action',255);
	      		t.string('notes',255);
	      		t.text('object_before');
	      		t.text('object_after');
	      		t.timestamps();
	      	//	t.unique(['user_id', 'fleet_id']);
  			});
  
};

exports.down = function(knex, Promise) {
  	  	return knex.schema.
	    dropTable('logging');
};
