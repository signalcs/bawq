
exports.up = function(knex, Promise) {
	return knex.schema.
	createTable('users_device',function(t){
  		t.integer('user_id').notNullable().unsigned().references('users.id');
  		t.integer('device_id').notNullable().unsigned().references('vehicles.id');
	});
};

exports.down = function(knex, Promise) {
  	return knex.schema.
    dropTable('users_device');
};
