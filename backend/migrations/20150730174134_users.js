
exports.up = function(knex, Promise) {
	return knex.schema.
	    createTable('users', function (t) {
	      	t.increments('id').notNullable().primary();
	      	t.string('email', 254).notNullable().unique();
	      	t.string('name', 254).notNullable();
	      	t.string('username', 50).notNullable().unique();
	      	t.string('password', 60).notNullable();
	      	t.integer('company_id').notNullable().unsigned().references('companies.id');
	      	t.string('role', 50).notNullable();
	      	t.text('permissions').notNullable();
	      	t.timestamps();

	    });
};

exports.down = function(knex, Promise) {
	return knex.schema.
	    dropTable('users');
};
