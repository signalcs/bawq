
exports.up = function(knex, Promise) {
  	  		return knex.schema.
	    createTable('driver_subscribe_vehicle', function (t) {
	      		t.integer('driver_id').notNullable().unsigned().references('drivers.id');
	      		t.integer('vehicle_id').notNullable().unsigned().references('vehicles.id');
	      		t.date('from_date').notNullable();
	      		t.date('to_date').notNullable();
	      		t.unique(['driver_id', 'vehicle_id','from_date']);
	    });
};

exports.down = function(knex, Promise) {
  	  	  	return knex.schema.
	    dropTable('driver_subscribe_vehicle');  
};
