
exports.up = function(knex, Promise) {
  	return knex.schema.
	    createTable('menus', function (t) {
	      	t.increments('id').notNullable().primary();
	      	t.string('name', 100).notNullable();
	      	t.string('icon', 100).notNullable();
	      	t.string('dest', 100).nullable();
	      	t.integer('parent_id').notNullable();
	      	t.string('module', 100).notNullable();
	      	t.timestamps();
	    });
};

exports.down = function(knex, Promise) {
  	return knex.schema.
	    dropTable('menus');
};
