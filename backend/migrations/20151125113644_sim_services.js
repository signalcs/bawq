
exports.up = function(knex, Promise) {
  		return knex.schema.
	    createTable('sim_services', function (t) {
	      		t.integer('service_id').notNullable().unsigned().references('services.id');
	      		t.integer('sim_card_id').notNullable().unsigned().references('sim_cards.id');
	      		t.unique(['service_id', 'sim_card_id']);
	    });
};

exports.down = function(knex, Promise) {
  	  	return knex.schema.
	    dropTable('sim_services');  
};
