
exports.up = function(knex, Promise) {
	  return knex.schema.
  			createTable('users_fleets',function(t){
  			//	t.increments('id').notNullable().primary();
	      		t.integer('user_id').notNullable().unsigned().references('users.id');
	      		t.integer('fleet_id').notNullable().unsigned().references('fleets.id');
	      		t.integer('company_id').notNullable().unsigned().references('companies.id');
	      		t.unique(['user_id', 'fleet_id']);
  			});
};

exports.down = function(knex, Promise) {
	  	return knex.schema.
	    dropTable('users_fleets');
};
