
exports.up = function(knex, Promise) {
    		return knex.schema.
	    createTable('drivers', function (t) {
	    		t.increments('id').notNullable().primary();
	    		t.string('name',50);
	    		t.string('license',50).notNullable();;
	    		t.date('license_expiration_date');
	    		t.string('phone',50).notNullable();
	    		t.string('email',50).notNullable();
	    		t.string('address',100).notNullable();
	    		t.string('refid',50);
	    		t.integer('company_id').notNullable().unsigned().references('companies.id');
	    		t.timestamps();
	      		//t.integer('sim_card_id').notNullable().unsigned().references('sim_cards.id');
	      		//t.unique(['service_id', 'sim_card_id']);
	    });
};

exports.down = function(knex, Promise) {
  	  	  	return knex.schema.
	    dropTable('drivers');  
};
