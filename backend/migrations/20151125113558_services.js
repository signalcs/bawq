
exports.up = function(knex, Promise) {
		return knex.schema.
	    createTable('services', function (t) {
	      	t.increments('id').notNullable().primary();
	      	t.string('service_name', 40).notNullable().unique();
	      	t.string('info', 20).notNullable();//roaming,standard
	    });
};

exports.down = function(knex, Promise) {
		  	return knex.schema.
	    dropTable('services');  
  
};
