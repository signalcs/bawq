
exports.up = function(knex, Promise) {
	return knex.schema.
	    createTable('companies', function (t) {
	      	t.increments('id').notNullable().primary();
	      	t.string('name', 254).notNullable().unique();
	      	t.string('address', 254).notNullable();
	      	t.string('email', 100).notNullable();
	      	t.string('tel', 20);
	      	t.string('Fax', 100);
	      	t.string('contact_person', 100);
	      	t.integer('status');
	      	t.timestamp('created_at').defaultTo(knex.fn.now());
	      	t.timestamp('updated_at');
	    });
};

exports.down = function(knex, Promise) {
	return knex.schema.
	    dropTable('companies');
};
