
exports.up = function(knex, Promise) {
	return knex.schema.
	    createTable('fleets', function (t) {
	      	t.increments('id').notNullable().primary();
	      	t.string('name', 100).notNullable();
	      	t.integer('company_id').notNullable().unsigned().references('companies.id');
	      	t.timestamps();
	    });
};

exports.down = function(knex, Promise) {
	return knex.schema.
	    dropTable('fleets');
};
