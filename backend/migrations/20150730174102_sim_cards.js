
exports.up = function(knex, Promise) {
		return knex.schema.
	    createTable('sim_cards', function (t) {
	      	t.increments('id').notNullable().primary();
	      	t.string('simcard_number', 40).notNullable().unique();
	      	t.string('type', 20).notNullable();//roaming,standard
	      	t.string('status', 20).notNullable().defaultTo('unused');//used or unused or transferred
	      	t.integer('company_id').unsigned().references('companies.id');
	      	t.timestamps();
	    });
};

exports.down = function(knex, Promise) {
	  	return knex.schema.
	    dropTable('sim_cards');  
};
