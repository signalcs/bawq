
exports.up = function(knex, Promise) {
	return knex.schema.
	    createTable('vehicles', function (t) {
	      	t.increments('id').notNullable().primary();
	      	t.string('systemno', 20).notNullable().unique();
	      	t.string('plateno', 50);
	      	t.string('simcard', 20).references('sim_cards.simcard_number').unique();
	      	t.string('timezone', 10);
	      	t.string('device_type', 10).notNullable();
	      	t.string('object_type', 50).notNullable();
	      	t.integer('fleet_id').unsigned().references('fleets.id');
	      	t.integer('company_id').notNullable().unsigned().references('companies.id');
	      	t.string('status',20).notNullable().defaultTo('provision');//provision , aproved , nocredit
	      	t.timestamps();
	    });
};

exports.down = function(knex, Promise) {
	return knex.schema.
	    dropTable('vehicles');
};
