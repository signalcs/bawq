var argv = require('minimist')(process.argv.slice(2));
var net = require('net');

var ActiveConnections = {};
var pingInterval = {};
var devices = {};
var clientCount = 7; // 5000
var heartbeatInterval = 25 * 1000;
var idx = 0;
var intervalID;

var server_opts = {
	host: 'localhost',
	port: 8090
}

var makeConnection = function() {
  	
  	var client = new net.Socket();
  	idx++;

  	client.connect(server_opts.port, server_opts.host, function(){
		console.log('Connected id no: '+ idx);
	});

  	ActiveConnections[idx] = client;
  	startPings(idx);

  	if (idx === clientCount) {
   	 	clearInterval(intervalID);
  	}
};

var startPings = function(idx){
    if(pingInterval[idx])clearInterval(pingInterval[idx]);

	pingInterval[idx] = setInterval(function(){
		ping(idx);
	}, heartbeatInterval);
}


var ping = function(idx){
	var device_uid = 13880000113 + idx;
	var current_device = makedeviceid(device_uid.toString());

    if(current_device in devices){
        devices[current_device] = MoveRandom(devices[current_device]);
    }else{
        devices[current_device] = generateRandomPositionInRegion();
    }
    
    location_word = convertLongLatToBCD(devices[current_device].latitude)+''+convertLongLatToBCD(devices[current_device].longitude);

	var data = '2424800025'+current_device+makedate()+location_word+'00000000C0470007A819280116A01D00FFE20D';

	var couldWrite = ActiveConnections[idx].write(data,'hex',function(){});
}

intervalID = setInterval(makeConnection, 25);

function makedeviceid(device_uid){

    var systemno = device_uid.substr(1); // remove trailing 1
    var ip_array = systemno.match(/.{1,2}/g); // group by 2
    var identifier = parseInt(ip_array[0]) - 30; // get identifier
    var ip = [];

    for(i=1; i < 5; i++){
        ip_array[i] = parseInt(ip_array[i], 10);
        
        if(identifier& Math.pow(2,4-i)){ // get identifier value
          if(ip_array[i] < 128){ // check and replace first bit
            ip_array[i]= ip_array[i] + 128;
          }
        }

        ip.push((ip_array[i].toString(16)).pad()); // push to array the hex string
    }
    return ip.join('');
}

String.prototype.pad = function(size) {
    var s = String(this);
    while (s.length < (size || 2)) {s = "0" + s;}
    return s;
}

function makedate(){
	var d = new Date();
	var year = d.getFullYear().toString().substr(2,2);
	var month = ((d.getMonth() + 1).toString()).pad();
	var date = (d.getDate().toString()).pad();
	var hours = (d.getHours().toString()).pad();
	var min = (d.getMinutes().toString()).pad();
	var sec = (d.getSeconds().toString()).pad();

	return year+month+date+hours+min+sec;
}

function convertLongLatToBCD(long){

    //var temp =  long.toString();
    //temp.explode
    var fraction =  parseInt((Math.abs(long) % 1)*60*1000);
    // get the fraction only and multiply it by 60 then by 1000 to move
    //the floating point .. then parse to integer to remove the left fractions 
    var INT = parseInt(Math.abs(long)); // converting to int and directly to base16 fraction is removed..
    if(long > 0)//positive .. 
    {
      //add most sig bit to refer it is positive ..
      INT += 0x80;
      return INT+''+(fraction.toString()).pad(5)+'';
    }
    return '0'+INT+''+(fraction.toString()).pad(5);
}

function generateRandomPositionInRegion(){
  MaxLat = -26.189948; 
  MinLong =  -50.420380;
  MinLat = -25.807309;
  MaxLong =  -50.620880;

    return {
        longitude : Math.random() * (MaxLong - MinLong) + MinLong,
        latitude  : Math.random()  * (MaxLat - MinLat) + MinLat,
    }
}
function MoveRandom(Coordinates){
    var step = 0.005;
    //generate Random from 1 to 4 ;
    var rand = Math.floor(Math.random() * (6 - 1 )) + 1;
    if(rand == 1 || rand == 3){
        Coordinates.latitude += step;
    }
    if(rand == 2 || rand == 1){
        Coordinates.longitude += step;
    }
    if(rand == 2 || rand == 4){
        Coordinates.latitude -= step;
    }
    if(rand == 3 || rand == 5){
        Coordinates.longitude -= step;
    }
    return Coordinates;
}