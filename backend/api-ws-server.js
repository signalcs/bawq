
var express     = require('express'),
    expressJwt  = require('express-jwt'),
    http        = require('http'),
    https       = require('https'),
    config      = require('config'),
    fs          = require("fs"),
    logger      = require('morgan'),
    bodyParser  = require('body-parser'),
    events      = require('./events'),
    bookshelf   = require('./model/bookshelf'),
    Model       = require('./model/models'),
    mongoose    = require('./model/mongoose'),
    redisio     = require('./redis-io'),
    app         = express();

var sslPort = process.env.HTTPS_PORT || 443;
var httpPort = process.env.HTTP_PORT || 9000;
var isSSL = config.get('sslServer');

// ssl certificates from comodo ssl
var options = {
    key: fs.readFileSync('./certs/bawq.enc.key'),
    cert: fs.readFileSync('./certs/www_bawq_com.crt'),
    ca: [
        fs.readFileSync('./certs/AddTrustExternalCARoot.crt'),
        fs.readFileSync('./certs/COMODORSAAddTrustCA.crt'),
        fs.readFileSync('./certs/COMODORSADomainValidationSecureServerCA.crt'),
        fs.readFileSync('./certs/COMODORSAExtendedValidationSecureServerCA.crt')
    ]
};

var httpServer = http.createServer(app).listen(httpPort, function(){
    if(!isSSL){ 
        console.log('Bawq.com HTTP server listening on port ' + httpServer.address().port); 
        redisio.socketIO(httpServer);
    }
});

if(isSSL){
    var httpsServer = https.createServer(options, app).listen(sslPort, function(){
        console.log('Bawq.com SSL server listening on port ' + httpsServer.address().port);
        redisio.socketIO(httpsServer);
    });
}

app.use(function(req, res, next){
    if (isSSL && !req.secure) {
        return res.redirect(301, 'https://'+ req.headers['host'] + req.url);
    } else {
        return next();
    }
});

app.use(logger('dev'));
app.use(bodyParser.json());

app.all('/*', function(req, res, next) {
    // CORS headers
    res.header("Access-Control-Allow-Origin", "*"); // restrict it to the required domain
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
    // Set custom headers for CORS
    res.header('Access-Control-Allow-Headers', 'Content-type,Accept,X-Access-Token,X-Key,Authorization');

    if (req.method == 'OPTIONS') {
        res.status(200).end();
    } else {
        next();
    }
});

// Auth Middleware - This will check if the token is valid
// Only the requests that start with /api/v1/* will be checked for the token.
// Any URL's that do not follow the below pattern should be avoided unless you 
// are sure that authentication is not needed
app.all('/api/v1/*', expressJwt({secret: config.get('jwt-secret')}));
// all api will route to routes middleware
app.use('/', require('./routes'));

// static files (js, css, etc)
app.use("/", express.static(__dirname + "/sites/webapp"));
// access control routes
app.all("/*", function(req, res, next) {
    res.sendFile("./index.html", { root: __dirname + "/sites/webapp" });
});

app.use(function (err, req, res, next) {
    if (err.name === 'UnauthorizedError') {
        res.status(401);
        res.json({
            "status": 401,
            "message": 'Token has expired.'
        });
        return;
    }
});

