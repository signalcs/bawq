var Model = require('../model/models.js');

var companies = {

    getAll: function(req, res) {
        
        if(req.user.role == 'admin'){
            Model.Company.fetchAll().then(function(collection){
                res.send(collection.toJSON());
            });
        }else{

            Model.Company.where('id', req.user.company_id).fetchAll().then(function(collection) {
                res.send(collection.toJSON());
            }).catch(function(err) {
                console.error(err);
            });
        }

    },
    getByCompanyId: function(req, res) {
        var company_id;
        if(req.user.role != 'admin')
        {
            company_id = req.user.company_id;
        }else
        {
            company_id = req.params.company_id;
        }
        Model.Company.query('where','id','=',company_id).fetchAll().then(function(collection){
            res.send(collection.toJSON());

        });
    },

    getOne: function(req, res) {
        var id = req.params.id;
        var company = data[0]; // Spoof a DB call
        res.json(company);
    },

    create: function(req, res) {
        if(req.user.role == 'admin' 
        || (req.user.role == 'moderator' && req.user.company_id == req.body.id)
        || (req.user.role == 'user' && req.user.company_id == req.body.id && 'add_company' in req.user.permissions)
        )       
        {
            delete req.body.id;
            new Model.Company()
                .save(req.body)
                .then(function(data)
                {
                    console.log(data.attributes);
                    //get it from the database to get fetch the id .. 
               //     Model.Company.query('where','company_id','=',req.user.company_id).
                 //       query('where','company_id','')
                    res.json({success:true,data:data.attributes, message:'The Company '+req.body.name+' ['+data.attributes.id+'] has been created succesfully.'})
                }).catch(function(){
                    res.json({success:false, message:'The Company '+req.body.name+' already exists.'})

                });
        }
    },

    update: function(req, res) {
        if(req.user.permissions == ''){
            var menu_role = {};
        }else
        {
            var menu_role = JSON.parse(req.user.permissions);
        }

        if(req.user.role == 'admin' 
            || (req.user.role == 'moderator' && req.user.company_id == req.body.id)
            || (req.user.role == 'user' && req.user.company_id == req.body.id && 'edit_company' in menu_role)
            )
        { 
            if(req.user.role == 'moderator' && req.user.role == 'user')
            {
               delete req.body.name;
            }
            new Model.Company({id: req.body.id})
                .save(req.body)
                .then(function(model)
                {
                    res.json({message:'The Company '+req.body.name+' ['+req.body.id+'] has been Edited succesfully.'})
                });
        }else
        {
            res.json({message:'You are Not allowed to edit Company Information'});
        }

    },

    delete: function(req, res) 
    {
     //   var id = req.params.id;
     //Only Admin can delete the Organization ,,
        if(req.user.role == 'admin')
        {     
            new Model.Company({id: req.body.id})
                .destroy()
                .then(function(model) {
                    res.json({success:false,message:'The Company '+req.body.name+' ['+req.body.id+'] has been Removed succesfully.'})
                }).catch( function()
                {
                    res.json({success:true, message:'The Company '+req.body.name+' ['+req.body.id+'] Cannot be removed Please Remove All Fleets related to it First.'});
                });
        }
    }
};

module.exports = companies;

