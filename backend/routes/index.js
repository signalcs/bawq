var express = require('express');
var router = express.Router();

var auth = require('./auth');
var vehicles = require('./vehicles');
var fleets = require('./fleets');
var menus = require('./menus');
var users = require('./users');
var companies = require('./companies');
var simcards = require('./simcards');
var dashboard = require('./dashboard');
var drivers = require('./drivers');
var playback = require('./reports/playback');


var trip_report = require('./trip_report');
/*
 * Routes that can be accessed by any one
 */
router.post('/api/login', auth.login);
// router.post('/login_backend', auth.login_backend);

/*
 * Routes that can be accessed only by autheticated users
 */
router.get('/api/v1/dashboard',dashboard.getAll);
 
// router.get('/api/v1/simcards');

router.get('/api/v1/simcards', simcards.getAll);
router.get('/api/v1/simcards/unused/:company_id', simcards.getUnused);
router.get('/api/v1/simcards/:number', simcards.getBySimCardNumber);

router.post('/api/v1/simcards/delete', simcards.delete);
router.post('/api/v1/simcards/update', simcards.update);
router.post('/api/v1/simcards/create',simcards.create);
router.post('/api/v1/simcards/transfer',simcards.transfer);
router.post('/api/v1/simcards/updateservices',simcards.setServices);

router.get('/api/v1/vehicles', vehicles.getAll);
router.get('/api/v1/vehicles/last', vehicles.getLastLocation);
router.get('/api/v1/vehicles/location', vehicles.getDeviceLocation);
router.get('/api/v1/vehicles/getinfo', vehicles.getDeviceInfo);
router.get('/api/v1/vehicles/todaymile', vehicles.getTodayMile);
router.get('/api/v1/vehicles/:fleet_id', vehicles.getByFleetId);

router.post('/api/v1/vehicles/delete', vehicles.delete);
router.post('/api/v1/vehicles/update', vehicles.update);
router.post('/api/v1/vehicles/create', vehicles.create);
router.post('/api/v1/vehicles/changestatus',vehicles.changestatus);


router.get('/api/v1/users', users.getAll);
router.get('/api/v1/users/:userid', users.getById);
router.post('/api/v1/users/delete', users.delete);
router.post('/api/v1/users/update', users.update);
router.post('/api/v1/users/assignfleets',users.assignFleets);
router.post('/api/v1/users/create',users.create);

router.get('/api/v1/mypermissions/',users.getMyPermissions);
router.post('/api/v1/users/permissions/',users.getPermissions);
router.post('/api/v1/users/setpermissions/',users.setPermissions);

router.get('/api/v1/companies', companies.getAll);
router.get('/api/v1/companies/:company_id', companies.getByCompanyId);
router.post('/api/v1/companies/delete', companies.delete);
router.post('/api/v1/companies/update', companies.update);
router.post('/api/v1/companies/create',companies.create);


router.get('/api/v1/fleets', fleets.getAll);
router.get('/api/v1/fleets/:company_id', fleets.getByCompanyId);

router.post('/api/v1/fleets/delete', fleets.delete);
router.post('/api/v1/fleets/update', fleets.update);
router.post('/api/v1/fleets/create', fleets.create);

router.get('/api/v1/drivers', drivers.getAll);
router.get('/api/v1/drivers/:company_id', drivers.getByCompanyId);
router.post('/api/v1/drivers/delete', drivers.delete);
router.post('/api/v1/drivers/update', drivers.update);
router.post('/api/v1/drivers/create', drivers.create);


router.get('/api/v1/menus', menus.getAll);

/**
 *
 * Reports Routes
 *
 */
router.get('/api/v1/trip-report', trip_report.generate);
router.post('/api/v1/playback', playback.generate);

module.exports = router;
