var Model = require('../model/models.js');

var simcards = {

    getAll: function(req, res) {
        //only admin should see the list of simcards..
            if(req.user.role == 'admin')
            {
                Model.SimCard.fetchAll({ withRelated: ['company','services']}).then(function(collection){
                    res.send(collection.toJSON());
                });
            }

    },

//simcards.getUnused
    getUnused: function(req, res) {
    //    var vehicle = new Model.Vehicle();
    //    vehicle.relate('fleet');
    //    if('fleet_id' in req.param){
    //        vehicle.query('fleet')
    //    }
        Model.SimCard.query('where','status','=','unused').query('where','company_id','=',req.params.company_id).fetchAll().then(function(collection){
            res.send(collection.toJSON());
        });

    },

    getBySimCardNumber: function(req,res) {
        Model.SimCard.query('where','simcard_number','=',req.params.number).fetchAll().then(function(collection){
            res.send(collection.toJSON());
        });

    },

    getOne: function(req, res) {
    //    var id = req.params.id;
    //    var vehicle = data[0]; // Spoof a DB call
     //   res.json(vehicle);
    },

    create: function(req, res) {
        //only Admin is allowed to add a phone number
        if(req.user.role == 'admin')     
        {
            //by default when creating a simcard status will set to unused ...
            req.body.status  =  'unused';
            //if there is id then delete it not to edit exiting 
            delete req.body.id;
            new Model.SimCard()
                .save(req.body)
                .then(function(model)
                {
                    res.json({data:model.attributes,message:'The SimCard '+req.body.simcard_number+' ['+req.body.id+'] has been created succesfully.'})
                });
        }
    },

    update: function(req, res) {
        //only Admin is allowed to edit a phone number
        delete req.body.company;
        delete req.body.services;
        if(req.user.role == 'admin')
        {    
            new Model.SimCard({id: req.body.id})
                .save(req.body)
                .then(function(model){
                  //  console.log(model.attributes);
                //  var Vehicle_message = '';
                        //status changed unlink the vehicle now... 
                       // Model.Vechile.query('where','simcard','=',model)
                    //   console.log(model.attributes.status);
                       if(model.attributes.status == 'unused' || model.attributes.status == 'transferred' )
                       {
                            Model.Vehicle.query('where','simcard','=',model.attributes.simcard_number).fetch().then(function(vehicle){
                                
                                if(vehicle != null)
                                {
                                    vehicle.attributes.simcard = null;
                                    vehicle.attributes.status = 'provision';
                                    new Model.Vehicle({id: vehicle.attributes.id}).save(vehicle.attributes).then(function(vehicle){


                                 //   console.log('saved');
                                     //   Vehicle_message  = '<br>The Vehicle System No. '+vehicle.systemno+' ['+vehicle.id+'] has been Unlinked succesfully.';
                                    });
                                }
                            });
                    
                       }
                        console.log({success:true ,message:'The SimCard  No. '+req.body.simcard_number+' ['+req.body.id+'] has been Edited succesfully.'});
                        res.json({success:true ,message:'The SimCard  No. '+req.body.simcard_number+' ['+req.body.id+'] has been Edited succesfully.'+Vehicle_message});
            }).catch(function(){
                    res.json({success:false ,message:'The SimCard No. '+req.body.simcard_number+' ['+req.body.id+'] cannot be Edited.'});

            });
        }

    },

    delete: function(req, res) {

        if(req.user.role == 'admin')
        {     
            new Model.SimCard({id: req.body.id})
                .destroy()
                .then(function(model) {
                    res.json({success:false,message:'The SimCard '+req.body.simcard_number+' ['+req.body.id+'] has been Removed succesfully.'})
                }).catch( function()
                {
                    res.json({success:false, message:'The SimCard '+req.body.simcard_number+' ['+req.body.id+'] Cannot be removed.'});
            });
        }

    },
    transfer: function(req, res) {
        if(req.user.role == 'admin')
        {
            delete req.body.company;
            delete req.body.created_at;
            delete req.body.updated_at;

            console.log(req.body);
            //first make sure that this sim card is not linked to vehicle .. 
            //if it is not linked tell the user first to change the status first to unused ,, it will be for sure unlinked automatically ,, 
            //else if it is unlinked simply change the company_id to the new and change the status to unused .. 
             Model.Vehicle.query('where','simcard','=',req.body.simcard_number).fetch().then(function(vehicle){ 
                console.log(vehicle);
                if(vehicle == null)
                {  
                console.log(vehicle);
                    new Model.SimCard({id:req.body.id}).save({company_id:req.body.company_id,status:'unused'}).then(function(model){
                    //success message .. 
                    res.json({success:true, message:'The SimCard '+req.body.simcard_number+' ['+req.body.id+'] Has Been successfuly transferred.'});
                    });
                }
             });
        }
    },
    listServices: function(req, res) {
     //   Model.
        Model.Service.fetchAll().then(function(service){
                res.send(collection.toJSON());
        });
    },
    setServices: function(req, res) {
        if(req.user.role == 'admin')
        {
            
            //drop all the current services for this company ..
            Model.SimServices.query('where','sim_card_id','=',req.body.simcard_id)
            .destroy()
            .then(function(model) {
                console.log(req.body.services);
                for(var i in req.body.services)
                    {
                        console.log(i);
                        new Model.SimServices().save({sim_card_id:req.body.simcard_id,service_id:i}).then(function(model){


                            Model.SimCard.query('where','id','=',req.body.simcard_id).fetch({ withRelated: ['company','services']}).then(function(collection){
                            res.send(collection.toJSON());
                           // console.log(collection.toJSON());
                            });


                       });
                    }
            }).catch(function(err)
            {
                console.log('err');
                console.log(err);

            });
            //console.log(req);
        }
    },
    getSimCardServices: function(req,res) {
        if(req.user.role == 'admin')
        {
            Model.Service.query('where','sim_card_id','=',req.body.sim_card_id).fetchall().then(function(services){

            });
        }
    }
};

module.exports = simcards;
