var Model = require('../model/models.js');

var drivers = {

    getAll: function(req, res) {
        var company_id;
        var DriverModel = Model.Driver;
        if(req.user.role != 'admin'){
            DriverModel = DriverModel.query('where','company_id','=',req.user.company_id);
        }
        DriverModel.fetchAll({ withRelated: ['company']}).then(function(collection){
            res.send(collection.toJSON());
        });
    },
    
    getByCompanyId: function(req, res) {
        var company_id;
        var DriverModel = Model.Driver;
        if(req.user.role != 'admin'){
            DriverModel = DriverModel.query('where','company_id','=',req.user.company_id);
        }
        DriverModel.fetchAll().then(function(collection){
            res.send(collection.toJSON());
        });
    },


    create: function(req, res) {
        req.body;
        if(req.user.role == 'admin' 
        || (req.user.role == 'moderator')
        || (req.user.role == 'user' && req.user.company_id == req.body.id && 'add_driver' in req.user.permissions)
        )       
        {
            if(req.user.role == 'moderator' || req.user.role == 'user')
            {
                req.body.company_id = req.user.company_id;
            }
            console.log(req.body);

            delete req.body.id;
            new Model.Driver()
                .save(req.body)
                .then(function(data)
                {
                    //console.log(data.attributes);
                    //get it from the database to get fetch the id .. 
               //     Model.Company.query('where','company_id','=',req.user.company_id).
                 //       query('where','company_id','')
                    res.json({success:true,data:data.attributes, message:'The driver '+req.body.name+' ['+data.attributes.id+'] has been created succesfully.'})
                }).catch(function(){
                    res.json({success:false, message:'The Driver '+req.body.name+' Cannot be added'})

                });
        }
    },

    update: function(req, res) {
        if(req.user.permissions == ''){
            var menu_role = {};
        }else
        {
            var menu_role = JSON.parse(req.user.permissions);
        }

        if(req.user.role == 'admin' 
            || (req.user.role == 'moderator' && req.user.company_id == req.body.id)
            || (req.user.role == 'user' && req.user.company_id == req.body.id && 'edit_driver' in menu_role)
            )
        { 
            delete req.body.company;
            new Model.Driver({id: req.body.id})
                .save(req.body)
                .then(function(model)
                {
                    res.json({message:'The Driver '+req.body.name+' ['+req.body.id+'] has been Edited succesfully.'})
                });
        }else
        {
           //res.json({message:'You are Not allowed to edit Company Information'});
        }

    },

    delete: function(req, res) 
    {
     //   var id = req.params.id;
     //Only Admin can delete the Organization ,,
        if(req.user.role == 'admin' 
            || (req.user.role == 'moderator' && req.user.company_id == req.body.id)
            || (req.user.role == 'user' && req.user.company_id == req.body.id && 'delete_driver' in menu_role)
            )
        {     
            new Model.Driver({id: req.body.id})
                .destroy()
                .then(function(model) {
                    res.json({success:true,message:'The Driver '+req.body.name+' ['+req.body.id+'] has been Removed succesfully.'})
                }).catch( function()
                {
                    res.json({success:false, message:'The Driver '+req.body.name+' ['+req.body.id+'] Cannot be removed.'});
                });
        }
    }
};

module.exports = drivers;

