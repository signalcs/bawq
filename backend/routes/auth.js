var jwt     = require('jsonwebtoken'),
    Promise = require('bluebird');
    Model   = require('../model/models.js');

var auth = {

    login: function(req, res) {
        // get authorization body
        var authorization = req.body.auth || '';
        // decode authorization headers from base64 to utf8
        var credentials = new Buffer(authorization, 'base64').toString('utf8');
        credentials = credentials.split(':');

        var username = credentials[0];
        var password = credentials[1];
        var remember = credentials[2];

        if(authorization == '' || username == '' || password == ''){
            res.status(401);
            res.json({
                "status": 401,
                "message": "Invalid credentials"
            });
            return;
        }

        auth.validate(username, password, function(err, user){
            if(user != null){
                var this_user = user.omit('password','created_at', 'updated_at');
                res.json(genToken(this_user, remember));
            }else{
                res.status(401);
                res.json({
                    "status": 401,
                    "message": err.message
                });
                return;
            }
        });
    },

    validate: function(username, password, callback) {
        // fetch user and test password verification
        Model.User.login(username, password)
        .then(function(user) {
            return callback(null, user);
        })
        .catch(function(err) {
            return callback(err, null);
        });
    }
}

// private method
function genToken(user, remember) {
    var expire = 60 * 60 * 2;

    if(remember) expire = '7d';

    var token = jwt.sign(
        user, 
        require('config').get('jwt-secret'), 
        { expiresIn: expire }
    );
    return {
        token: token
    };
}

module.exports = auth;
