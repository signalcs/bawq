var Model   = require('../model/models.js'),
    _       = require('lodash');

var fleets = {

    getAll: function(req, res) {
        
        if(req.user.role == "moderator"){

            Model.Fleet.where('company_id', req.user.company_id).fetchAll({
                withRelated: [{
                    company : function(qb){
                        qb.select('companies.id', 'companies.name');
                    }
                }]
            })
            .then(function(collection){
                res.send(collection.toJSON());
            })
            .catch(function(err){
                if(err) console.log(err);
            });

        }
        else if(req.user.role == "admin"){

            Model.Fleet.fetchAll({
                withRelated: [{
                    company : function(qb){
                        qb.select('companies.id', 'companies.name');
                    }
                }]
            })
            .then(function(collection){
                res.send(collection.toJSON());
            })
            .catch(function(err){
                if(err) console.log(err);
            });

        }
        
    },

    getByCompanyId: function(req,res) {
        if(req.user.role == 'admin')
        {            
            Model.Fleet.query('where','company_id','=',req.params.company_id).fetchAll({ withRelated: ['company']}).then(function(collection){
            res.send(collection.toJSON());
         //   console.log(collection.toJSON());
        });
        }else{
            fleets.getAll(req,res);
        }
    },

    create: function(req, res) {
        if(req.user.role == 'admin' 
        || (req.user.role == 'moderator')
        || (req.user.role == 'user' && req.user.company_id == req.body.id && 'add_fleet' in req.user.permissions)
        )       
        {
            if(req.user.role == 'moderator' && req.user.role == 'user')
            {
                req.body.company_id = req.user.company_id;
            }

            delete req.body.id;
            if(req.user.role != 'admin')
            {
                //overWrite the data companyId to current login user company id 
                // so he is not able to create fleet to different company ..
                req.body.company_id = req.user.company_id;
            }
            new Model.Fleet()
                .save(req.body)
                .then(function(data)
                {
                    //save log
                    new Model.Logging()
                    .save(
                        {
                            user_id:req.user.id,
                            company_id:req.body.company_id,
                            model_id:data.attributes.id,
                            model_name:'Fleet',
                            action:'create',
                            notes: 'Creates',
                            object_after: JSON.stringify(data.attributes)
                          //  object_after: 'adham ayman'
                        }).then(function(data){});
                    Model.Fleet.query('where','id','=',data.attributes.id).fetch({ withRelated: ['company']}).then(function(collection){
                  //  res.send(collection.toJSON());
                    //   console.log(collection.toJSON());
                        res.json({data:collection.toJSON() ,message:'The Fleet '+req.body.name+' ['+data.attributes.id+'] has been created succesfully.'})
                    });

                });
        }
    },

    update: function(req, res) {
        // if(req.user.permissions == ''){
        //     var permissions = {};
        // }else
        // {
        //     var permissions = JSON.parse(req.user.permissions);
        // }

        if(req.body.company) delete req.body.company;

        if(req.user.role == "admin"){

            new Model.Fleet({id: req.body.id})
            .save(req.body)
            .then(function(model){
                new Model.Logging()
                .save({
                    user_id: req.user.id,
                    company_id: req.body.company_id,
                    model_id: model.attributes.id,
                    model_name: 'Fleet',
                    action: 'edit',
                    notes: 'update',
                    object_after: JSON.stringify(model.attributes)
                }).then(function(data){});

                res.json({success: true, message:'Group has been updated succesfully.'});
            });

        }else if(req.user.role == 'moderator'){

            new Model.Fleet({id: req.body.id})
            .save(req.body)
            .then(function(model){
                new Model.Logging()
                .save({
                    user_id: req.user.id,
                    company_id: req.body.company_id,
                    model_id: model.attributes.id,
                    model_name: 'Fleet',
                    action: 'edit',
                    notes: 'updates',
                    object_after: JSON.stringify(model.attributes)
                }).then(function(data){});

                res.json({success: true, message:'Group has been updated succesfully.'});
            });

        }else if(req.user.role == 'user' && 'edit_fleet' in permissions ){
            //select all fleets related to users .. 
            new Model.UserFleet({user_id: req.user.id,fleet_id: req.body.id }).fetch().then(function(UF_model){
               var obj = UF_model.toJSON();
               if(obj != {})
               {
                console.log(req.body.company_id);
                    delete req.body.company_id;
                    new Model.Fleet({id: req.body.id})
                        .save(req.body)
                        .then(function(model){
                            new Model.Logging()
                                .save(
                            {
                                user_id:req.user.id,
                                company_id:req.body.company_id,
                                model_id:model.attributes.id,
                                model_name:'Fleet',
                                action:'edit',
                                notes: 'updates',
                                object_after: JSON.stringify(model.attributes)
                            }).then(function(data){});
                            res.json({message:'The Fleet '+req.body.name+' ['+model.attributes.id+'] has been Edited succesfully.'})
                    });

               }
            });

        }
    },

    delete: function(req, res) {

        if(req.user.role == 'admin' || (req.user.role == 'moderator')){
            new Model.Fleet({id: req.body.id})
            .destroy()
            .then(function(model) {
                new Model.Logging()
                .save({
                    user_id: req.user.id,
                    company_id: req.body.company_id,
                    model_id: req.body.id,
                    model_name: 'Fleet',
                    action: 'delete',
                    notes: 'Removes',
                    object_after: JSON.stringify(req.body)
                }).then(function(data){});

                res.json({ success: true, message:'Group has been deleted succesfully.'})

            }).catch( function(){

                res.json({ success: false, message:'Group '+req.body.name+' cannot be deleted please unlink all vehicles related.'});

            });
        }
    }
};

module.exports = fleets;
