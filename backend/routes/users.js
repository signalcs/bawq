var Model       = require('../model/models'),
    Bookshelf   = require('../model/bookshelf'),
    _           = require('lodash'),
    Promise     = require('bluebird'),
    bcrypt      = Promise.promisifyAll(require('bcrypt'));

var User = {
    
    getAll: function(req, res) {

        if(req.user.role == 'admin'){

            Model.User.query(function(qb){
                qb.select('id', 'email', 
                    'name', 'username', 
                    'company_id', 'role', 
                    'permissions', 'created_at', 
                    'updated_at'
                );

                qb.where('username', '!=', 'admin');
            })
            .fetchAll({
                withRelated: [{
                    fleets : function(qb){
                        qb.select('fleets.id', 'fleets.name');
                    },
                    company : function(qb){
                        qb.select('companies.id', 'companies.name')
                    }
                }]
            })
            .then(function(collection){
                res.send(collection.toJSON());
            })
            .catch(function(err){
                if(err) console.log(err);
            });

        }else if(req.user.role == 'moderator'){

            Model.User.query(function(qb){
                qb.select('id', 'email', 
                    'name', 'username', 
                    'company_id', 'role', 
                    'permissions', 'created_at', 
                    'updated_at'
                );

                qb.where('company_id', '=', req.user.company_id)
                .andWhere('role', '!=', 'admin');
            })
            .fetchAll({
                withRelated: [{
                    fleets : function(qb){
                        qb.select('fleets.id', 'fleets.name');
                    },
                    company : function(qb){
                        qb.select('companies.id', 'companies.name')
                    }
                }]
            })
            .then(function(collection){
                res.send(collection.toJSON());
            })
            .catch(function(err){
                if(err) console.log(err);
            });

        }

    },

    getById: function(req, res) {
        
        Model.User.where('username','=',req.params.userid).fetch().then(function(collection){
            var data = collection.toJSON();
            delete data.password;
            res.send(data);
        });

    },

    create: function(req, res) {
        //only admin who would be able to create user .. 
        if(req.user.role == 'admin'||
            req.user.role == 'moderator' ||
            (req.user.role == 'user' && 'create_user' in req.user.permissions) )

        {
            console.log(req.body.role);
            if(req.user.role == 'admin' && !('role' in req.body))
            {
                req.body.role = 'user';
            }
            if(req.user.role == 'moderator' || req.user.role == 'user')
            {
                req.body.company_id = req.user.company_id;
                req.body.role = 'user';
            }

            console.log(req.body);
            //for security reason we delete them 
            //to make sure no body would path data through their parameters ..
            delete req.body.id;
            req.body.permissions = "{}";
         //   req.body.password 
         bcrypt.genSalt(10, function(err, salt) {
            bcrypt.hash(req.body.password , salt, function(err, hash) {
                req.body.password = hash;
        // Store hash in your password DB.
                new Model.User()
                    .save(req.body)
                    .then(function(model)
                    {
                        delete model.attributes.password;
                        new Model.Logging()
                                .save(
                            {
                                user_id:req.user.id,
                                company_id:req.body.company_id,
                                model_id:model.attributes.id,
                                model_name:'User',
                                action:'create',
                                notes: 'Creates',
                                object_after: JSON.stringify(model.attributes)
                            }).then(function(data){});

                        new Model.User({id:model.attributes.id}).fetch({withRelated: ['fleets','company']}).then(function(user){
                            res.json({success:true,user:user,message:'The  User '+model.attributes.name+' ['+model.attributes.email+'] has been created succesfully.'});
                        });
                    }).catch(function(a){
                            res.json({success:false,message:'The  User '+req.body.name+' ['+req.body.email+'] cannot be created User Or Email should be Unique.'});
                    });
                });
        });
        }
    },

    update: function(req, res) {

        if(req.user.role == "admin" || req.user.role == "moderator"){
            // for security reason we delete them to make sure no
            // body would path data through their parameters ..
            var params = _.omit(req.body, ['password', 'permissions', 'role', 'company', 'fleets']);  

            // validation is also present in the client side
            // but it is always better to do validation on the server
            var isNameEmpty = _.isNil(params.name);
            var isUsernameEmpty = _.isNil(params.username);
            var isEmail = validate(params.email);

            if(!isNameEmpty && !isUsernameEmpty && isEmail){
                new Model.User({ id : params.id })
                .save(params)
                .then(function(model){
                    res.json({
                        success:true,
                        message:'User has been updated succesfully.'
                    });
                })
                .catch(function(err){
                    if(err) console.log(err);
                    res.json({
                        success: false,
                        message: 'Something went wrong.'
                    });
                });
            }else if(!isEmail){
                res.json({
                    success: false,
                    message: 'Please enter a correct email address.'
                });
            }else{
                res.json({
                    success: false,
                    message: 'Please fill required fields.'
                });
            }
            
        }
    },

    delete: function(req, res) {

        if(req.user.role == "admin" || req.user.role == "moderator"){

            var params = req.body;

            if(req.user.role == "moderator" && (params.role == "moderator" || params.role == "admin")){

                res.json({
                    success: false,
                    message: 'Cannot delete a moderator or admin account.'
                });

            }else{

                new Model.User({ id: params.id })
                .destroy()
                .then(function(model) {
                    res.json({
                        success: true, 
                        message:'User has been deleted succesfully.'
                    });
                })
                .catch(function(){
                    res.json({
                        success: false,
                        message: 'Cannot delete user please unlink groups related to this user.'
                    });
                });

            }
            
        }

    },

    assignFleets:function(req,res) {
        
        var params = _.pick(req.body, ['id', 'checked', 'company_id']);
        var userFleets = [];

        if(req.user.role == "moderator" || req.user.role == "admin"){
            Model.UserFleet.where('user_id', params.id).destroy()
            .then(function(model){

                _.each(params.checked, function(value, key){ 
                    if(value){
                        userFleets.push({ user_id : params.id, fleet_id : parseInt(key), company_id : params.company_id });
                    }
                });

                Bookshelf.transaction(function(t) {
                    return Promise.map(userFleets, function(userfleet){
                        return new Model.UserFleet(userfleet).save(null, {transacting: t});
                    });

                }).then(function(userfleet) {
                    res.json({
                        success: true,
                        message: 'Groups has been successfully assigned.'
                    });
                }).catch(function(err) {
                    res.json({
                        success: false,
                        message: 'Something went wrong.'
                    });
                });

            });
        }

    },

    getMyPermissions:function(req,res)
    {
        if(req.user.role == 'user')
        {
            Model.User.query('where','id','=',req.user.id).fetch().then(function(model){
                console.log(model.attributes.permissions);
                res.json(model.attributes.permissions);
            });
        }
    },

    getPermissions:function(req,res)
    {
      //  req.params.user_id
        if(req.user.role == 'admin' ||
            req.user.role == 'moderator' && req.user.company_id == req.body.company_id)
        {

            //prevent different company moderator from getting the permissions of the user ..
            var company_id = req.body.company_id;
            if(req.user.role == 'moderator')
            {
                company_id = req.user.company_id;
            }

            Model.User.query('where','company_id','=',company_id).query('where','id','=',req.body.id).fetch().then(function(model){
                res.json(model.attributes.permissions);
            });
        }
   //     console.log(req.body.company_id);
    },
    setPermissions:function(req,res)
    {
              //  req.params.user_id
        if(req.user.role == 'admin' ||
            req.user.role == 'moderator' && req.user.company_id == req.body.user.company_id)
        {
            //prevent different company moderator from getting the permissions of the user ..
            var company_id = req.body.user.company_id;
            if(req.user.role == 'moderator')
            {
                company_id = req.user.company_id;
            }
        }
        //permissions
       // req.body
       new Model.User({id:req.body.user.id,company_id:company_id})
                .save({permissions:JSON.stringify(req.body.UserPermissions)})
                .then(function(model){
                       new Model.Logging()
                                .save(
                            {
                                user_id:req.user.id,
                                company_id:company_id,
                                model_id:model.attributes.id,
                                model_name:'User',
                                action:'changePermissions',
                                notes: 'change the permissions of',
                                object_after: JSON.stringify(req.body.UserPermissions)
                            }).then(function(data){});

                  //  console.log(model);
                  res.json(model.attributes.permissions);
                });
    }
};

var tester = /^[-!#$%&'*+\/0-9=?A-Z^_a-z{|}~](\.?[-!#$%&'*+\/0-9=?A-Z^_a-z`{|}~])*@[a-zA-Z0-9](-?\.?[a-zA-Z0-9])*\.[a-zA-Z](-?[a-zA-Z0-9])+$/;

function validate(email)
{
    if (!email)
        return false;
        
    if(email.length>254)
        return false;

    var valid = tester.test(email);
    if(!valid)
        return false;

    // Further checking of some things regex can't handle
    var parts = email.split("@");
    if(parts[0].length>64)
        return false;

    var domainParts = parts[1].split(".");
    if(domainParts.some(function(part) { return part.length>63; }))
        return false;

    return true;
}


module.exports = User;
