var Model       = require('../model/models.js');
var Mongoose    = require('mongoose'),
    _           = require('lodash'),
    gps_log     = Mongoose.model('gps_log'),
    moment      = require('moment');

var vehicles = {

    getAll: function(req, res) {

        if(req.user.role == 'user' || req.user.role == 'moderator')
        {
            Model.UsersDevice.query('where','user_id', req.user.id).fetchAll().then(function(userDevices){
                userDevices = userDevices.toJSON();
                userDevices = _.map(userDevices, 'device_id');

                // fetch all vehicles in users_device array
                Model.Vehicle.where('id', 'in', userDevices)
                .fetchAll({columns : [
                    'id', 'systemno', 'plateno', 'simcard', 'timezone', 
                    'device_type', 'object_type', 'fleet_id', 'company_id', 
                    'status'
                ]})
                .then(function(collection){
                    res.send(collection.toJSON());
                });
            });
        }
        else if(req.user.role == 'admin')
        {

            Model.Vehicle.fetchAll({
                withRelated: [{
                    company : function(qb){
                        qb.select('companies.id', 'companies.name');
                    },
                    fleet: function(qb){
                        qb.select('fleets.id', 'fleets.name');
                    }
                }]
            })
            .then(function(collection){
                res.send(collection.toJSON());
            });

        }

    },

    /**
     * Get Last Saved Location from mongodb database
     * @param {array} systemnos
     */
    getLastLocation: function(req, res){
        var params = req.query;
        var systemnos = params.systemno.split(',');

        /*  gps_log database query on last location */
        gps_log.aggregate(
            { $match : {'systemno': {$in: systemnos}, 'locate': true } },
            { $group : 
                { 
                    _id: '$systemno',
                    systemno: { $last: '$systemno' }, 
                    location: { $last: '$location' }, 
                    time: { $last: '$time' },
                    speed: { $last: '$speed' },
                    orientation: { $last: '$orientation'},
                    locate: { $last: '$locate'},
                    accstatus: { $last: '$accstatus'}
                }
            },
            { $sort : {'time': -1} },
            function(err, doc){
                if(doc != null) res.send(doc);
            }
        );
    },

     /**
     * Get Device last location from mongodb database
     * @param {array} systemnos
     */
    getDeviceLocation: function(req, res){
        var params = req.query;
        var systemnos = params.systemno.split(',');

        gps_log.aggregate(
            { $match : {'systemno': {$in: systemnos}, 'locate': true } },
            { $group : 
                { 
                    _id: '$systemno',
                    systemno: { $last: '$systemno' }, 
                    location: { $last: '$location' }, 
                    time: { $last: '$time' }
                }
            },
            { $project : { "_id":0, 
                "systemno": 1,
                "location": 1
            }},
            { $sort : {'time': -1} },
            function(err, doc){
                if(doc != null) res.send(doc);
                else res.send({systemno: params.systemno, location: false});
            }
        );
    },

    /**
     * Get Last Saved Location from mongodb database
     * @param {array} systemnos
     */
    getDeviceInfo: function(req, res){
        var params = req.query;
        var systemno = params.systemno;

        gps_log.findOne({$query: {'systemno': systemno}, $orderby: {'time': -1}}, { _id:0 }, function(err, doc){
            if(doc != null) res.send(doc);
        });
    },

    /**
     * Get Today Mileage from mongodb database
     * @param {string} systemno
     */
    getTodayMile: function(req, res){
        var params = req.query;
        var systemno = params.systemno;

        // should change this utc offset based on clients or device timezone
        var start = moment().utcOffset("+03:00").startOf('day').format('YYYY-MM-DD HH:mm:ss'); 
        var end = moment().utcOffset("+03:00").format('YYYY-MM-DD HH:mm:ss'); 

        gps_log.find({'systemno': systemno, 'time': {$gte: start, $lt: end} }, { _id:0 }, function(err, doc){
            
            var last_mile           = -1,
                last_time           = 0,
                maxspeed_config     = 200, // should be on config
                minspeed_config     = 2, // should be on config
                mile                = 0;

            for (var i = 0; i < doc.length; i++) {
                var velocity            = doc[i].speed,
                    locate              = doc[i].locate,
                    accstatus           = parseInt(doc[i].accstatus),
                    start_mile          = doc[i].mileage,
                    start_time          = moment(doc[i].time);

                // will compute for the mileage
                if(velocity > minspeed_config && locate && accstatus > 0){
                    if(last_mile >= 0){
                        var val = start_mile - last_mile;
                        if(val > 0){
                            var travel_time = start_time.unix() - last_time; // travel time in seconds
                            if((val / 1000) / (travel_time / 3600) <= maxspeed_config){
                                mile += val;
                            }
                        }
                    }
                    last_mile = start_mile;
                    last_time = start_time.unix();
                }else{
                    lastmile = -1;
                }
            }
            
            res.send({mile: mile/1000});
        });
    },

    getByFleetId: function(req,res) {
        Model.Vehicle.query('where','fleet_id','=',req.params.fleet_id).fetchAll().then(function(collection){
            res.send(collection.toJSON());
        });

    },

    getOne: function(req, res) {
        var id = req.params.id;
        var vehicle = data[0]; // Spoof a DB call
        res.json(vehicle);
    },

    create: function(req, res) {
        if(req.user.role == 'admin'
        ||(req.user.role == 'moderator' )
        ||(req.user.role == 'user'  && 'add_vehicle' in req.user.permissions ))
        {
            console.log('create');
            if(req.user.role == 'moderator' || req.user.role == 'user')
            {
                req.body.company_id = req.user.company_id;
            }
            req.body['status'] = 'provision';
            new Model.Vehicle()
                .save(req.body)
                .then(function(model){
                    Model.Vehicle.query('where','id','=',model.attributes.id).fetch({ withRelated: ['company','fleet']}).then(function(collection){
                        //res.send(collection.toJSON());
                        //save log 
                        new Model.Logging()
                                .save(
                            {
                                user_id:req.user.id,
                                company_id:req.body.company_id,
                                model_id:model.attributes.id,
                                model_name:'Vehicle',
                                action:'create',
                                notes: 'Creates',
                                object_after: JSON.stringify(model.attributes)
                            }).then(function(data){});
                    });
                    //save log
                    res.json({data:model ,message:'The Vehicle '+model.attributes.systemno+' ['+model.attributes.id+'] has been created succesfully.'})

      //      res.json({body:req.body,message:'The Vehicle System No. '+req.body.systemno+' ['+req.body.id+'] has been Added succesfully.'})
        });      
        }
    },

    update: function(req, res) {
        if(req.user.role == 'admin'
        ||(req.user.role == 'moderator' && req.user.company_id == req.body.company_id)
        ||(req.user.role == 'user' && req.user.company_id == req.body.company_id && 'edit_vehicle' in req.user.permissions ))
        {
            delete req.body.fleet;
            delete req.body.company;
            //we need to check for the simcard number in the vechile table if it is exists and its relation 
            //if it is used by other vehicle don't accept the connection ,, if it is transferred or unused change the 
            //status of the simcard 
            new Model.Vehicle({id: req.body.id})
                .save(req.body)
                .then(function(model){
                        new Model.Logging()
                                .save(
                            {
                                user_id:req.user.id,
                                company_id:req.body.company_id,
                                model_id:model.attributes.id,
                                model_name:'Vehicle',
                                action:'edit',
                                notes: 'Updates',
                                object_after: JSON.stringify(req.body)
                            }).then(function(data){});
                   //     res.json({data:model.attributes ,message:'The Vehicle '+model.attributes.Systemno+' ['+model.attributes.id+'] has been created succesfully.'});
                    
                    //change the status of the simcard to used ..
                    Model.SimCard.query('where','simcard_number','=',req.body.simcard).fetch().then(function(vehicle){
                        new Model.SimCard({id:vehicle.attributes.id}).save({status:'used'},{patch:true}).then(function(model){
                        });

                    });
            res.json({body:req.body,message:'The Vehicle System No. '+req.body.systemno+' ['+req.body.id+'] has been Edited succesfully.'})
        });      
        }
    },

    delete: function(req, res) {
       // var id = req.params.id;
      //  data.splice(id, 1) // Spoof a DB call
     //   res.json(true);
        console.log(req.body.id);
        new Model.Vehicle({id: req.body.id})
        .destroy()
        .then(function(model) {
            //if vehicle is successfully deleted change the status of the related simcard
                        new Model.Logging()
                                .save(
                            {
                                user_id:req.user.id,
                                company_id:req.body.company_id,
                                model_id:req.body.id,
                                model_name:'Vehicle',
                                action:'delete',
                                notes: 'Removes',
                                object_after: JSON.stringify(req.body)
                            }).then(function(data){});

            Model.SimCard.query('where','simcard_number','=',req.body.simcard).fetch().then(function(simCard)
            {
                new Model.SimCard({id:simCard.attributes.id}).save({status:'unused'},{patch:true}).then(function(model){});
            }).catch( function(aaa){});

            res.json({success:true,data:req.body,message:'The Vehicle '+req.body.systemno+' ['+req.body.id+'] has been Removed succesfully.'})
    }).catch(function(aaa){
            res.json({success:false,message:'The Vehicle '+req.body.Systemno+' ['+req.body.id+'] Cannot be removed.'});
    });

    },
    changestatus: function(req,res) {
   //     console.log(req.body);
      if(req.user.role == 'admin')
      {
        delete req.body.fleet;
        delete req.body.company;

        new Model.Vehicle({id: req.body.id})
            .save({status: req.body.status})
            .then(function(model){
                        new Model.Logging()
                                .save(
                            {
                                user_id:req.user.id,
                                company_id:req.body.company_id,
                                model_id:model.attributes.id,
                                model_name:'Vehicle',
                                action:'changestatus',
                                notes: 'Change the status to '+model.attributes.status+' of',
                                object_after: JSON.stringify(req.body)
                            }).then(function(data){});
                // console.log(model);
                res.json({body:req.body,message:'Status of '+req.body.systemno+' ['+req.body.id+']  has been changed to.'+req.body.status});
            });
      }

    }
};

module.exports = vehicles;
