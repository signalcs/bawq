var Mongoose    = require('mongoose'),
    moment      = require('moment'),
    request 	= require('request'),
    _           = require('lodash'),
    queue 		= require('queue');

var gps_log     = Mongoose.model('gps_log');


var playback = {
	generate: function(req, res){
		var q = queue();
		var results = [];

		var params = req.body;
		var start = moment(params.start).format('YYYY-MM-DD HH:mm:ss'); 
        var end = moment(params.end).format('YYYY-MM-DD HH:mm:ss');

        var projection = { 
		    __v: false,
		    devicestatus: false,
		    temperature: false,
		    orientation: false,
		    oil: false,
		    voltage: false,
		    digitalout: false
		};

        gps_log.find({'systemno': params.systemno, 'locate': true,  'time': {$gte: start, $lt: end}}, projection,
        	function(err, result){

        		var reportResult = {};
				var lastMile = -1,
				lastTime = 0, 
				maxSpeed = 200,
				minSpeed = 2,
				resultMile = 0,
				Mile = 0;

				_.forEach(result, function(value, key) {
					// convert immutable mongoose
					// response to object
					value = value.toObject();

				  	var velocity = value.speed,
				  	locate = value.locate,
				  	accStatus = parseInt(value.accstatus),
				  	startMile = value.mileage,
				  	startTime = moment(value.time);

				  	if(velocity > minSpeed && locate && accStatus > 0){

				  		if(lastMile >= 0){
	                        var val = startMile - lastMile;
	                        if(val > 0){
	                            var travelTime = startTime.unix() - lastTime; // travel time in seconds
	                            if((val / 1000) / (travelTime / 3600) <= maxSpeed){
	                                Mile += val;
	                                value.mile = Mile;
	                                
	                                
									q.push(function(cb) {  	
									  	request.get({url: 'https://ewatrack.com/nominatim/reverse', qs: { 
									  		format: 'json',
						                    lat: value.location[1],
						                    lon: value.location[0],
						                    zoom: 16,
						                    addressdetails: 1,
						                    namedetails: 1
						                },
						                headers: {
										    'Accept-Language': 'en-US'
										}
						            }, function (error, response, body) {
										  	if (!error && response.statusCode == 200) {
										    	body = JSON.parse(body);
										    	value.address = body.display_name;

									    		results.push(value);
								  				cb();	 	
										  	}
										});
									});

	                            }
	                        }
	                    }
	                    lastMile = startMile;
	                    lastTime = startTime.unix();

				  	}else{
				  		lastMile = -1;
				  	}
				});

				q.start(function(err) {
					if(err) console.log(err);
				  	results = _.orderBy(results, ['time']);
				  	res.send(results);
				});
        	}
        );

		// gps_log.paginate({'systemno': params.systemno, 'locate': true,  'time': {$gte: start, $lt: end}}, 
		// 	{ 
		// 		page: params.page, 
		// 		limit: params.limit,
		// 		select: 'systemno time location speed locate mileage accstatus'
		// 	}, function(err, result) {

		// 		var reportResult = {};
		// 		var lastMile = -1,
		// 		lastTime = 0, 
		// 		maxSpeed = 200,
		// 		minSpeed = 2,
		// 		resultMile = 0,
		// 		Mile = 0;

		// 		reportResult.docs = [];

		// 		_.forEach(result.docs, function(value, key) {
		// 			// convert immutable mongoose
		// 			// response to object
		// 			value = value.toObject();

		// 		  	var velocity = value.speed,
		// 		  	locate = value.locate,
		// 		  	accStatus = parseInt(value.accstatus),
		// 		  	startMile = value.mileage,
		// 		  	startTime = moment(value.time);

		// 		  	var isLaston = (accStatus > 0 && key == result.docs.length - 1);
		// 		  	var isTripFinish = time.format("YYYY-MM-DD") != lastDay || ((trip.accstatus == 0) && (Stopped == false)) || isLaston;

		// 		  	if(velocity > minSpeed && locate && accStatus > 0){

		// 		  		if(lastMile >= 0){
	 //                        var val = startMile - lastMile;
	 //                        if(val > 0){
	 //                            var travelTime = startTime.unix() - lastTime; // travel time in seconds
	 //                            if((val / 1000) / (travelTime / 3600) <= maxSpeed){
	 //                                Mile += val;

	 //                                value.mile = Mile;
		// 		  					reportResult.docs.push(value);
	 //                            }
	 //                        }
	 //                    }
	 //                    lastMile = startMile;
	 //                    lastTime = startTime.unix();

		// 		  	}else{	
		// 		  		lastMile = -1;
		// 		  	}

		// 		});

		// 		reportResult.total = result.total;
		// 		reportResult.limit = result.limit;
		// 		reportResult.page = result.page;
		// 		reportResult.pages = result.pages;

		// 		res.send(reportResult);
		// });

	}
}

module.exports = playback;