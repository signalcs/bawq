var Mongoose    = require('mongoose'),
    moment      = require('moment'),
    _           = require('lodash');

var gps_log     = Mongoose.model('gps_log');

var trip_report = {
    generate: function(req, res) {
        var params = req.query;
        var systemnos = params.systemno.split(',');

        gps_log.aggregate(
            { $match : {'systemno': {$in: systemnos}, 'time': {$gte: new Date(params.from), $lt: new Date(params.to) } } },
            { $group : 
                { 
                    _id: {
                        systemno: '$systemno', 
                        location: '$location',
                        time: '$time',
                        speed: '$speed',
                        orientation: '$orientation',
                        locate: '$locate',
                        accstatus: '$accstatus',
                        mileage: '$mileage'
                    }
                }
            },
            { $sort: { '_id.time': 1} },
            { $group :
                {
                    _id: '$_id.systemno',
                    trip: {
                        $push: {
                            systemno: '$_id.systemno', 
                            location: '$_id.location', 
                            time: '$_id.time',
                            speed: '$_id.speed',
                            orientation: '$_id.orientation',
                            locate: '$_id.locate',
                            accstatus: '$_id.accstatus',
                            mileage: '$_id.mileage'
                        }
                    }
                }
            },
            function(err, doc){
                if (!doc.length) {
                    res.send(doc);
                }else{
                    var results = [];

                    for (var i = 0; i < doc.length; i++) {
                        results.push(generateTrip(doc[i]))
                    }
                    res.send(results);
                }
            }
        );

        function generateTrip(doc){

            var Stopped = true;
            var maxSpeed = 0;
            var lastDay = moment(doc.trip[0].time).format("YYYY-MM-DD");
            var reportResult = {};
            var Mile = 0, Odometer = 0, lastMile = -1,
                totalMile = 0, lastTime = 0, timeStop = 0, startTime,
                startAddress, Idle = true, idleStartTime = 0, runningToIdle = 0,
                totalDrive = 0, totalIdle = 0, tripMaxSpeed = 0, maxDriveTime = 0, maxIdleTime = 0;

            reportResult.result = [];

            for (var i = 0; i < doc.trip.length; i++) {
                var trip = doc.trip[i];
                var time = moment(trip.time);
                var isLaston = (trip.accstatus > 0 && i == doc.trip.length - 1);
                var isTripFinish = time.format("YYYY-MM-DD") != lastDay || ((trip.accstatus == 0) && (Stopped == false)) || isLaston;

                // get maxspeed of the trip
                if(trip.speed > maxSpeed && trip.locate && trip.accstatus > 0) maxSpeed = trip.speed;
                // will compute for the mileage
                if(trip.speed > 2 && trip.locate && trip.accstatus > 0){
                    if(lastMile >= 0){
                        var val = trip.mileage - lastMile;
                        if(val > 0){
                            var travelTime = time.unix() - lastTime; // travel time in seconds
                            if((val / 1000) / (travelTime / 3600) <= 200){
                                Mile += val;
                                totalMile += val;
                                Odometer = trip.mileage;
                            }
                        }
                    }
                    lastMile = trip.mileage;
                    lastTime = time.unix();
                }else{
                    lastMile = -1;
                }

                // get the location of the start of the trip
                if (trip.accstatus > 0 && Stopped){
                    if (trip.locate){
                        startAddress = trip.location;
                    }else{
                        startAddress = false;
                    }
                    startTime = time.unix();
                }
                // get the time idle time or stop time
                if (isTripFinish && i > 0) timeStop = timeStop + (time.unix() - startTime);

                // From the running state to the stop state
                if (trip.speed <= 2 && Idle == false){
                    runningToIdle = runningToIdle + (moment(doc.trip[i - 1].time).unix() - idleStartTime);
                    Idle = true;
                }
                // Driving state
                if (trip.speed > 2){
                    if (Idle){ // From the resting state to the running state
                        idleStartTime = time.unix();
                    }
                    if (isTripFinish || (i == doc.trip.length - 1)){ // Driving to the end of the line
                        runningToIdle = runningToIdle + (time.unix() - idleStartTime);
                        idleStartTime = time.unix();
                    }
                    Idle = false;
                }
                // get end result when trip is finish
                if (isTripFinish || (i == doc.trip.length - 1)){
                    if (isTripFinish && (timeStop > 0) && Mile > 0){
                        var start = moment(startTime*1000).toISOString();
                        var arriveTime = moment(doc.trip[isLaston ? i : i - 1].time).toISOString();

                        var idleDuration = moment.duration((timeStop - runningToIdle) * 1000).humanize();
                        var driveDuration = moment.duration(runningToIdle * 1000).humanize();
                        
                        reportResult.result.push({
                            'date'          : lastDay,
                            'startTime'     : start,
                            'startAddress'  : startAddress,
                            'destTime'      : arriveTime,
                            'destAddress'   : doc.trip[isLaston ? i : i - 1].location,
                            'distance'      : Mile/1000,
                            'driveTime'     : driveDuration,
                            'idleTime'      : idleDuration,
                            'maxSpeed'      : maxSpeed,
                            'odometer'      : Odometer/1000
                        });
                        
                        totalDrive = totalDrive + runningToIdle;
                        totalIdle = totalIdle + (timeStop - runningToIdle);
                        tripMaxSpeed = maxSpeed > tripMaxSpeed ? maxSpeed : tripMaxSpeed;
                        maxDriveTime = runningToIdle > maxDriveTime ? runningToIdle : maxDriveTime;
                        maxIdleTime = (timeStop - runningToIdle) > maxIdleTime ? (timeStop - runningToIdle) : maxIdleTime;
                    }

                    // Daily Summary
                    if (time.format("YYYY-MM-DD") != lastDay || (i == doc.trip.length - 1))
                    {
                        if (totalDrive > 0 && totalMile > 0){
                            reportResult.systemno = trip.systemno;
                            reportResult.totalMile = totalMile/1000;
                            reportResult.totalDrive = moment.duration(totalDrive*1000).humanize();
                            reportResult.totalIdle = moment.duration(totalIdle*1000).humanize();
                            reportResult.tripMaxSpeed = tripMaxSpeed;
                            reportResult.maxDriveTime = moment.duration(maxDriveTime*1000).humanize();
                            reportResult.maxIdleTime = moment.duration(maxIdleTime*1000).humanize();
                        }
                        totalMile = 0;
                        timeStop = 0;
                        runningToIdle = 0;
                        Mile = 0;
                        maxSpeed = 0;
                        Stopped = true;
                        Idle = true;  
                    }

                    if(isTripFinish){
                        timeStop = 0;
                        runningToIdle = 0;
                        Mile = 0;
                        maxSpeed = 0;
                        Idle = true;
                    }
                }

                if(time.format("YYYY-MM-DD") == lastDay) Stopped = (trip.accstatus == 0);

                lastDay = time.format("YYYY-MM-DD");
            }

            return reportResult;           
        }
        
    }
};

module.exports = trip_report;