// Update with your config settings.
module.exports = {

    development: {
        client: 'mysql',
        connection: {
            host: '127.0.0.1',
            port: '3306',
            user: 'root',
            password: 'root',
            database: 'gps_baseinfo',
            charset: 'utf8'
        },
        migrations: {
            tableName: 'migrations'
        }
    },

    production: {
        client: 'mysql',
        connection: {
            host: '127.0.0.1',
            port: '3306',
            user: 'root',
            password: 'root',
            database: 'gps_baseinfo',
            charset: 'utf8'
        },
        migrations: {
            tableName: 'migrations'
        }
    }

};
