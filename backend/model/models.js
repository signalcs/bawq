var bookshelf = require('./bookshelf'),
	Promise  = require('bluebird'),
	validator = require('validator'),
	bcrypt = Promise.promisifyAll(require('bcrypt')),
	events = require('../events');

var Company,Menus,User,Vehicle,UserFleet,SimCard;

Company = bookshelf.Model.extend({
	tableName: 'companies',
	hasTimestamps: true,
	fleets : function() {
		return this.hasMany(Fleet);
	},
});

UserFleet = bookshelf.Model.extend({
	tableName: 'users_fleets',
	fleets : function() {
		return this.belongsTo(Fleet);
	},
	emitChange: function emitChange(event, data) {
        events.emit('userfleet' + '.' + event, data);
    },
});


var Fleet = bookshelf.Model.extend({
	tableName: 'fleets',
	hasTimestamps: true,
	vehicles : function() {
		return this.hasMany(Vehicle);
	},
	company : function()  {
		return this.belongsTo(Company);
	},
	users : function()  {
		 return this.belongsToMany(User,'users_fleets');
	},
	emitChange: function emitChange(event, data) {
        events.emit('fleet' + '.' + event, data);
    },
});

var UsersDevice = bookshelf.Model.extend({
	tableName: 'users_device',
	vehicle : function() {
		return this.belongsTo(Vehicle);
	},
	emitChange: function emitChange(event, data) {
        events.emit('user_device' + '.' + event, data);
    },
});


SimCard = bookshelf.Model.extend({
	tableName: 'sim_cards',
	hasTimestamps: true,
	company : function()  {
		return this.belongsTo(Company);
	},
	services : function()  {
			 return this.belongsToMany(Service,'sim_services');
			//return this.hasMany(Service,'sim_services');
	}
});


Menus = bookshelf.Model.extend({
	tableName: 'menus',
	hasTimestamps: true,
});

//	Fleet  = require('./fleets');

// password creation validation
// we can add more validations
// in this function in future.
function validatePasswordLength(password) {
    return validator.isLength(password, 8);
}
// will hash password using bcrypt
// run this before saving password
// to database.
function generatePasswordHash(password) {
    // Generate a new salt
    return bcryptGenSalt().then(function (salt) {
        // Hash the provided password with bcrypt
        return bcryptHash(password, salt);
    });
}

User = bookshelf.Model.extend({
	tableName: 'users',
	hasTimestamps: true,
	fleets 	: function()  {
	 //return this.belongsToMany(Fleet.Fleet,'users_fleets');
		return this.belongsToMany(Fleet,'users_fleets');

	},
	company : function()  {
		return this.belongsTo(Company);
	},
	emitChange: function emitChange(event) {
        events.emit('user' + '.' + event, this);
    },

    initialize: function initialize() {
        bookshelf.Model.prototype.initialize.apply(this, arguments);

        this.on('created', function onCreated(model) {
            model.emitChange('added');

            // active is the default state, so if status isn't provided, this will be an active user
            if (!model.get('status') || _.contains(activeStates, model.get('status'))) {
                model.emitChange('activated');
            }
        });
		/*
        this.on('updated', function onUpdated(model) {
        	console.log('updated')
            model.statusChanging = model.get('status') !== model.updated('status');
            model.isActive = _.contains(activeStates, model.get('status'));

            if (model.statusChanging) {
                model.emitChange(model.isActive ? 'activated' : 'deactivated');
            } else {
                if (model.isActive) {
                    model.emitChange('activated.edited');
                }
            }

            model.emitChange('edited');
        });
        */
        this.on('destroyed', function onDestroyed(model) {
           // if (_.contains(activeStates, model.previous('status'))) {
                model.emitChange('deactivated');
       //     }

            model.emitChange('deleted');
        });
    },

} , {

	  	login: Promise.method(function(email, password) {
		    if (!email || !password) throw new Error('Email and password are both required');

		    return new this({email: email.toLowerCase().trim()}).fetch().tap(function(user) {
		    	if(user == null) throw new Error('Username and password you entered don\'t match.');
		      	return bcrypt.compareAsync(password, user.get('password'))
		       	.then(function(res) {
		         	if (!res) throw new Error('Username and password you entered don\'t match.');
		       	});
		    });
		})
  	}
);

Vehicle = bookshelf.Model.extend({
	tableName: 'vehicles',
	hasTimestamps: true,
	fleet : function() {
		return this.belongsTo(Fleet);
	},
	company : function()  {
		return this.belongsTo(Company);
	},
	emitChange: function emitChange(event, data) {
        events.emit('vehicles' + '.' + event, data);
    },
	initialize: function initialize() {
        bookshelf.Model.prototype.initialize.apply(this, arguments);

        this.on('updated', function onUpdated(model) {

        	new Vehicle({'id': model.attributes.id}).fetch().then(function(edited_vehicle){
        		model.emitChange('edited', edited_vehicle);
        	});

        });
        this.on('created', function onUpdated(model) {

        	new Vehicle({'id': model.attributes.id}).fetch().then(function(created_vehicle){
        		model.emitChange('added', edited_vehicle);
        	});

        });
    },

});

Logging = bookshelf.Model.extend({
	tableName: 'logging',
	hasTimestamps: true,
	creator: function()
	{
		return this.belongsTo(User);
	},
	model: function()
	{
		return this.morphOne('model',Vehicle,User,Fleet,Company);
	},
	company : function()  {
		return this.belongsTo(Company);
	},
});


Service = bookshelf.Model.extend({
	tableName: 'services',
});


SimServices = bookshelf.Model.extend({
	tableName: 'sim_services',

});


Driver = bookshelf.Model.extend({
	tableName: 'drivers',
	hasTimestamps: true,
	company : function()  {
		return this.belongsTo(Company);
	},

});

DriverSubscribeVehicle = bookshelf.Model.extend({
	tableName: 'driver_subscribe_vehicle',

});



module.exports = {
	Logging : Logging,
	Company : Company,
	Fleet : Fleet,
	UsersDevice: UsersDevice,
	Menus : Menus,
	User: User,
	Vehicle : Vehicle,
	UserFleet: UserFleet,
	SimCard:SimCard,
	Service:Service,
	SimServices:SimServices,
	Driver:Driver,
	DriverSubscribeVehicle:DriverSubscribeVehicle
};


