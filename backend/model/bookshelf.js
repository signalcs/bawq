
var dbconfig = require('config').get('mysql');
var knex = require('knex')(dbconfig);

module.exports = require('bookshelf')(knex);