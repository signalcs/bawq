var config = require('config'), 
    Mongoose = require('mongoose'),
    mongoosePaginate = require('mongoose-paginate');

// mongo db configurations
var mongoDB = config.get('mongodb');
// from mongoose docs enable keepAlive to prevent
// "connection closed" error for long running apps
var options = { server: { socketOptions: { keepAlive: 1 } } };

var connect = function () {
    Mongoose.connect(mongoDB, options, function(err) {
        if (err) console.log(err);
        console.log('Successfully connected to gps_server database!');
    });
};
// run connect method
connect();

// when disconnected run connect method again
Mongoose.connection.on('disconnected', connect);

var Schema = Mongoose.Schema;
var gps_log_schema = Schema({
    systemno: { type: String, required: true}, // unique
    time: { type: Date, default: Date.now},
    location: {type: [Number], index: '2d'},
    speed: Number,
    orientation: Number,
    locate: Boolean,
    digitalout: Array,
    accstatus: String,
    devicestatus : Number,
    oil: Number,
    voltage: Number,
    mileage: Number,
    temperature: String
});

gps_log_schema.plugin(mongoosePaginate);

gps_log_schema.index({systemno: 1, time: 1});
// the schema is useless so far
// we need to create a model using it
var gps_log = Mongoose.model('gps_log', gps_log_schema);

// make this available to our users in our Node applications
module.exports = {
    gps_log : gps_log
};

