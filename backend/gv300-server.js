
var gps         = require("gps-tracking"),
    config      = require("config"),
    _           = require('lodash'),
    redis       = require('redis');

/* Configuration variables */
var device_opts = config.get('queclink_gv300');
var redis_auth = config.get('redis-auth');

redisClient = redis.createClient();
redisPub = redis.createClient();
redisSub = redis.createClient();

if(redis_auth){
    redisClient.auth(redis_auth);
    redisPub.auth(redis_auth);
    redisSub.auth(redis_auth);
}

/**
 *
 * Main GPS
 * Devices listener
 *
 */

redisClient.smembers('vehiclesClients', function(err, reply){
    if(err) console.log(err);

    var vehicles = eval('([' + reply.toString() + '])');
    vehicles = _.indexBy(vehicles, 'systemno');

    // listening to commands send by clients
    redisSub.subscribe('at:command:GV300');
    redisSub.on('message', function(channel, message){
        message = JSON.parse(message);
        server.emit('at:command', message);
    });

    server = gps.server(device_opts,function(device,connection){        
        /* Location event */
        device.on("resp:location",function(data){
            if(data.systemno in vehicles && vehicles[data.systemno].status == 'approved'){
                var device_timezone = (vehicles[data.systemno] != undefined) ? vehicles[data.systemno].timezone : '+0000';
                data.time = data.time+device_timezone;

                redisPub.publish(data.systemno, JSON.stringify(data));
            }
        });
        /* Command response event */
        device.on("ack:command", function(data){
            if(data.systemno in vehicles && vehicles[data.systemno].status == 'approved'){
                redisPub.publish(data.systemno, JSON.stringify(data));
            }
        });
    });    
});
