
(function(){
	'use strict';

	angular
		.module('controller.Group', [])
		.controller('GroupController', GroupController);

	GroupController.$inject = ['$scope', '$window', 'toaster', 'Groups', 'Users', 'ENV'];
	function GroupController($scope, $window, toaster, Groups, Users, ENV){
		var vm = this;
		vm.user = Users.getCurrentUser();

		Groups.getAllGroup(function(groups){
        	vm.groups = groups;
        	console.log(groups)
        });

        vm.groupPermissions = function(){
        	if(vm.user.role == "admin" || vm.user.role == "moderator") return true;
        	else return false;
        }

        vm.showEditGroup = function(group){
        	vm.group = group;
        }

        vm.editGroup = function(group){
        	Groups.editGroup(group, function(response){

        		Groups.getAllGroup(function(groups){
		        	vm.groups = groups;
		        });

        		toaster.pop('success', 'Update', response.message);
        	});
        }

        vm.deleteGroup = function(group){
        	Groups.deleteGroup(group, function(response){
        		
        		Groups.getAllGroup(function(groups){
		        	vm.groups = groups;
		        });

        		toaster.pop('error', 'Update', response.message);
        	});
        }
	}

})();