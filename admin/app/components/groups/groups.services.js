(function(){

    'use strict';

    angular
        .module('services.Group', [])
        .factory('Groups', Groups);

    Groups.$inject = ['$http', 'ENV'];
    function Groups($http, ENV) {
        
        return {
            getAllGroup: function(callback){
                $http.get(ENV.api_url + '/api/v1/fleets').then(function (groups) {
                    callback(groups.data);
                });
            },
            editGroup: function(data, callback){
                $http({method: 'POST', url: ENV.api_url + '/api/v1/fleets/update', data: data}).then(function(groups){
                    callback(groups.data)
                });
            },
            deleteGroup: function(data, callback){
                $http({method: 'POST', url: ENV.api_url + '/api/v1/fleets/delete', data: data}).then(function(groups){
                    callback(groups.data)
                });
            }

        };
    }

})();