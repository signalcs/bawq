(function(){

	'use strict';

	angular
		.module('controller.Sidebar', [])
		.controller('SidebarController', SidebarController);

	SidebarController.$inject = ['$scope', '$http', 'ENV'];
	function SidebarController($scope, $http, ENV){
		var vm = this;

		$http.get(ENV.api_url + '/api/v1/menus').then(function(menus){
            vm.menus = menus.data;
        });
	}

})();
