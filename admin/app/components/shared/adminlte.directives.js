(function () {
	'use strict';

	angular
		.module('directives.Adminlte', [])
		.directive('adminLteLayout', adminLteLayoutDirective);

	function adminLteLayoutDirective($window) {
		return {
			restrict: 'A',
			link: link,
			scope: {}
		};

		function link($scope, $element, $attrs) {
			$element.height($window.innerHeight - angular.element('.main-header').outerHeight() - 
				angular.element('.main-footer').outerHeight());

			angular.element($window).bind('resize', function(){
            	$element.height($window.innerHeight - angular.element('.main-header').outerHeight() - 
				angular.element('.main-footer').outerHeight());
            });
		}
	}

})();