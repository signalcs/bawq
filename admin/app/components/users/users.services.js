(function(){

    'use strict';

    angular
        .module('services.Users', [])
        .factory('Users', Users);

    Users.$inject = ['$http', '$window', 'ENV'];
    function Users($http, $window, ENV) {

        // convert 64bit encoded jwt
        // token to json object 
        function base64_decode(str) {
            var output = str.replace('-', '+').replace('_', '/');
            switch (output.length % 4) {
                case 0:
                    break;
                case 2:
                    output += '==';
                    break;
                case 3:
                    output += '=';
                    break;
                default:
                    throw 'Illegal base64url string!';
            }
            return window.atob(output); //polifyll https://github.com/davidchambers/Base64.js
        }

        return {
            getAllUsers: function(callback){
                $http.get(ENV.api_url + '/api/v1/users').then(function (users) {
                    callback(users.data);
                });
            },
            editUser: function(data, callback){
                $http({method: 'POST', url: ENV.api_url + '/api/v1/users/update', data: data}).then(function(user){
                    callback(user.data);
                });
            },
            assignGroup: function(data, checked, callback){
                data.checked = checked;
                $http({method: 'POST', url: ENV.api_url + '/api/v1/users/assignfleets', data: data}).then(function(user){
                    callback(user.data);
                });
            },
            deleteUser: function(data, callback){
                $http({method: 'POST', url: ENV.api_url + '/api/v1/users/delete', data: data}).then(function(user){
                    callback(user.data);
                });
            },
            getCurrentUser: function(){
                var token = $window.localStorage.token.split('.')[1];
                return JSON.parse(base64_decode(token));
            }
        };
    }

})();