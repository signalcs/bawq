
(function(){
	'use strict';

	angular
		.module('controller.Users', [])
		.controller('UsersController', UsersController);

	UsersController.$inject = ['$scope', '$http', 'DTOptionsBuilder', 'DTColumnDefBuilder', 'Users', 'Groups', 'toaster', 'ENV'];
	function UsersController($scope, $http, DTOptionsBuilder, DTColumnDefBuilder, Users, Groups,toaster, ENV){
		var vm = this;
		vm.user = Users.getCurrentUser();

		vm.dtOptions = DTOptionsBuilder.newOptions();

		vm.dtColumnDefs = [
          	DTColumnDefBuilder.newColumnDef(7).notSortable()
       	];

		vm.showEditUser = function(user){
			vm.selected = user;
		};

		vm.editUser = function(user){
			Users.editUser(user, function(response){
				notifier(response);
				refreshContent();
			});
		};

		vm.deleteUser = function(user){
			Users.deleteUser(user, function(response){
				notifier(response);
				refreshContent();
			});
		};

		vm.assignGroup = function(user){
			Users.assignGroup(user, vm.checkedGroup, function(response){
				notifier(response);
				refreshContent();
			});
		};

		vm.isChecked = function(id){
			if(vm.selected){
				console.log(id)
				angular.forEach(vm.selected.fleets, function(value, key){
					// console.log(value.id == id)
					if(value.id == id) return true;
					else return false;
				});
			}
		}

		$scope.$on('$destroy', function(){
			vm = null;
		});

		Users.getAllUsers(function(users){
			vm.users = users;
		});

		Groups.getAllGroup(function(groups){
			vm.groups = groups;
		});

		function notifier(response){
			if(response.success) toaster.pop('success', 'Update', response.message);
			else toaster.pop('error', 'Update', response.message);
		}

		function refreshContent(){
			Users.getAllUsers(function(users){
				vm.users = users;
			});
		}
	}

})();