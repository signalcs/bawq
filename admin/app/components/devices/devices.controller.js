
(function(){
	'use strict';

	angular
		.module('controller.Devices', [])
		.controller('DevicesController', DevicesController);

	DevicesController.$inject = ['$scope', '$window', '$http', 'Devices', 'toaster', 'ENV'];
	function DevicesController($scope, $window, $http, Devices, toaster, ENV){
		var vm = this;

		Devices.getAllDevices(function(devices){
			vm.devices = devices;
		});

        var token = $window.localStorage.token.split('.')[1];
        vm.user = JSON.parse(base64_decode(token));

        vm.devicePermissions = function(){
        	if(vm.user.role == "admin" || vm.user.role == "moderator") return true;
        	else return false;
        }

		// convert 64bit encoded jwt
		// token to json object 
		function base64_decode(str) {
        	var output = str.replace('-', '+').replace('_', '/');
	        switch (output.length % 4) {
	            case 0:
	                break;
	            case 2:
	                output += '==';
	                break;
	            case 3:
	                output += '=';
	                break;
	            default:
	                throw 'Illegal base64url string!';
	        }
	        return window.atob(output); //polifyll https://github.com/davidchambers/Base64.js
	    }
	}

})();