(function(){

    'use strict';

    angular
        .module('services.Devices', [])
        .factory('Devices', Devices);

    Devices.$inject = ['$http', 'ENV'];
    function Devices($http, ENV) {
        
        return {
            getAllDevices: function(callback){
                $http.get(ENV.api_url + '/api/v1/vehicles').then(function (devices) {
                    callback(devices.data);
                });
            },
            // editGroup: function(data, callback){
            //     $http({method: 'POST', url: ENV.api_url + '/api/v1/fleets/update', data: data}).then(function(groups){
            //         callback(groups.data)
            //     });
            // },
            // deleteGroup: function(data, callback){
            //     $http({method: 'POST', url: ENV.api_url + '/api/v1/fleets/delete', data: data}).then(function(groups){
            //         callback(groups.data)
            //     });
            // }

        };
    }

})();