
(function(){
	'use strict';

	angular
		.module('controller.Drivers', [])
		.controller('DriversController', DriversController);

	DriversController.$inject = ['$scope', '$http','toaster', 'ENV'];
	function DriversController($scope, $http, toaster, ENV){
		var vm = this;

		$http.get(ENV.api_url + '/api/v1/drivers').then(function (drivers) {
       		vm.drivers = drivers.data;
        });
	}

})();