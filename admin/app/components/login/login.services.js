(function(){

	'use strict';

	angular
		.module('services.Login', [])
		.factory('AuthenticationService', AuthenticationService)
		.factory('UserAuthenticationService', UserAuthenticationService)
		.factory('TokenInterceptor', TokenInterceptor);

	AuthenticationService.$inject = ['$window'];
	function AuthenticationService($window) { 
	    var auth = {
	        Authenticated: false,
	        Check: function() {
		    	if ($window.localStorage.token) {
		        	this.Authenticated = true;
		      	} else {
		        	this.Authenticated = false;
		        	delete this.user;
		      	}
		    }
	    }
	    return auth;
	}

	UserAuthenticationService.$inject = ['$http', '$location', '$window', 'AuthenticationService', 'ENV'];
	function UserAuthenticationService($http, $location, $window, AuthenticationService, ENV){
		return {
			signIn: function(username, password, remember) {
	            return $http.post(ENV.api_url + '/api/login', { auth : window.btoa(username+":"+password+":"+remember) });
	        },
	        logOut: function() {
      			if (AuthenticationService.Authenticated) {

			        AuthenticationService.Authenticated = false;
			        delete $window.localStorage.token;

			        $location.path(ENV.api_url + "/login");
      			}
	        },
		}
	}

	TokenInterceptor.$inject = ['$q', '$window', '$location', 'AuthenticationService', 'ENV'];
	function TokenInterceptor($q, $window, $location, AuthenticationService, ENV) {
	    return {
	        request: function (config) {
	            config.headers = config.headers || {};
	            if ($window.localStorage.token) {
	            	config.headers.Authorization = 'Bearer ' + $window.localStorage.token;
	            }
	            return config;
	        },
	        requestError: function(rejection) {
	            return $q.reject(rejection);
	        },
	        response: function (response) {
	            if (response != null && response.status == 200 && $window.localStorage.token && !AuthenticationService.isAuthenticated) {
	                AuthenticationService.isAuthenticated = true;
	            }
	            return response || $q.when(response);
	        },
	        responseError: function(rejection) {
	            if (rejection != null && rejection.status === 401 && ($window.localStorage.token || AuthenticationService.isAuthenticated)) {
	                delete $window.localStorage.token;
	                AuthenticationService.isAuthenticated = false;
	                $location.path(ENV.api_url + "/login");
	            }
	            return $q.reject(rejection);
	        }
	    }
	}

})();
