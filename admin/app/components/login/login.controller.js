(function(){

	'use strict';

	angular
		.module('controller.Login', ['toaster'])
		.controller('LoginController', LoginController);

	LoginController.$inject = [
		'$scope', 
		'$location', 
		'$window', 
		'AuthenticationService', 
		'UserAuthenticationService',
        'toaster',
        'ENV'
	];
	function LoginController($scope, $location, $window, AuthenticationService, UserAuthenticationService, toaster, ENV){
		$scope.signIn = function signIn(email, password, remember) {

            if (email != null && password != null) {

            	$scope.dataLoading = true;
            	if(!remember) remember = false;

                UserAuthenticationService.signIn(email, password, remember).success(function(data) {
                	$scope.dataLoading = false;
                    AuthenticationService.Authenticated = true;
                    // save to localstorage tokens data and user info
                    $window.localStorage.token = data.token;
			        // redirect to tracker
                    $location.path(ENV.base_url + "/dashboard");
                }).error(function(status, data) {
                	$scope.dataLoading = false;
                    toaster.pop("error", "Unauthorized", status.message);
                });
            }
        }

        $scope.$on('$destroy', function(){
        });
	}

})();
