'use strict';

/**
 * @ngdoc overview
 * @name adminApp
 * @description
 * # adminApp
 *
 * Main module of the application.
 */
angular
    .module('adminApp', [
        'ngAnimate',
        'ngMessages',
        'ngRoute',
        'ngTouch',
        'datatables',
        'config',
        'controller.Login',
        'services.Login',
        'controller.Dashboard',
        'controller.Sidebar',
        'controller.Organization',
        'services.Organization',
        'controller.Group',
        'services.Group',
        'controller.Devices',
        'services.Devices',
        'controller.Users',
        'services.Users',
        'controller.Drivers',
        'directives.Adminlte'
    ])
    .config(config)
    .run(run);

    config.$inject = ['$routeProvider', '$httpProvider', '$locationProvider', 'ENV'];
    function config($routeProvider, $httpProvider, $locationProvider, ENV) {
        $routeProvider
            .when(ENV.base_url+'/login', {
                templateUrl: 'components/login/login.html',
                controller: 'LoginController',
                title: 'Login Page',
                bodyclass: 'login-page'
            })
            .when(ENV.base_url+'/dashboard', {
                templateUrl: 'components/dashboard/dashboard.html',
                controller: 'DashboardController',
                controllerAs: 'dashboard',
                title: 'Dashboard',
                bodyclass: 'skin-blue sidebar-mini fixed'
            })
            .when(ENV.base_url+'/organization', {
                templateUrl: 'components/organization/organization.html',
                controller: 'OrgController',
                controllerAs: 'organizations',
                title: 'Organization',
                bodyclass: 'skin-blue sidebar-mini fixed'
            })
            .when(ENV.base_url+'/groups', {
                templateUrl: 'components/groups/groups.html',
                controller: 'GroupController',
                controllerAs: 'groups',
                title: 'Groups',
                bodyclass: 'skin-blue sidebar-mini fixed'
            })
            .when(ENV.base_url+'/devices', {
                templateUrl: 'components/devices/devices.html',
                controller: 'DevicesController',
                controllerAs: 'devices',
                title: 'Devices',
                bodyclass: 'skin-blue sidebar-mini fixed'
            })
            .when(ENV.base_url+'/users', {
                templateUrl: 'components/users/users.html',
                controller: 'UsersController',
                controllerAs: 'users',
                title: 'Users',
                bodyclass: 'skin-blue sidebar-mini fixed'
            })
            .when(ENV.base_url+'/drivers', {
                templateUrl: 'components/drivers/drivers.html',
                controller: 'DriversController',
                controllerAs: 'drivers',
                title: 'Drivers',
                bodyclass: 'skin-blue sidebar-mini fixed'
            })
            .otherwise({
                redirectTo: ENV.base_url+'/login'
            });

        $locationProvider.html5Mode(true);
        $httpProvider.interceptors.push('TokenInterceptor');
    }

    run.$inject = ['$rootScope', '$route', '$location', '$window', 'AuthenticationService', 'ENV'];
    function run($rootScope, $route, $location, $window, AuthenticationService, ENV){
        $rootScope.$on("$routeChangeStart", function(event, nextRoute, currentRoute) {
            // check if authenticated when refreshed
            // or when redirected to other page
            AuthenticationService.Check();
            //redirect only if both isAuthenticated is false and no token is set
            if (!AuthenticationService.Authenticated && !$window.localStorage.token) {
                $location.path(ENV.base_url+"/login");
            }else{
                // var token = $window.localStorage.token.split('.')[1];
                // var user = JSON.parse(url_base64_decode(token));

                // $rootScope.name = user.name;
                // $rootScope.company = user.company_id;

                // $location.path(ENV.base_url+"/dashboard");
            }
        });

        $rootScope.$on("$routeChangeSuccess", function(currentRoute, previousRoute){
            //Change page title, based on Route information
            $rootScope.title = $route.current.title;
            $rootScope.bodyclass = $route.current.bodyclass;
        });
    }

    //this is used to parse the profile
    function url_base64_decode(str) {
        var output = str.replace('-', '+').replace('_', '/');
        switch (output.length % 4) {
            case 0:
                break;
            case 2:
                output += '==';
                break;
            case 3:
                output += '=';
                break;
            default:
                throw 'Illegal base64url string!';
        }
        return window.atob(output); //polifyll https://github.com/davidchambers/Base64.js
    }
