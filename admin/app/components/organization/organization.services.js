(function(){

    'use strict';

    angular
        .module('services.Organization', [])
        .factory('Organization', Organization);

    Organization.$inject = ['$http', 'ENV'];
    function Organization($http, ENV) {
        
        return {
            getAllOrganization: function(callback){
                $http.get(ENV.api_url + '/api/v1/companies').then(function (companies) {
                    callback(companies.data);
                });
            }

        };
    }

})();