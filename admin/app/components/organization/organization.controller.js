
(function(){
	'use strict';

	angular
		.module('controller.Organization', [])
		.controller('OrgController', OrgController);

	OrgController.$inject = ['$scope', '$http','toaster', '$window','Organization', 'ENV'];
	function OrgController($scope, $http, toaster, $window,Organization, ENV){
		var vm = this;
		var token = $window.localStorage.token.split('.')[1];
        vm.user = JSON.parse(base64_decode(token));

		Organization.getAllOrganization(function(companies){
			vm.companies = companies;
		});

        vm.orgPermissions = function(){
        	if(vm.user.role == "admin" || vm.user.role == "moderator") return true;
        	else return false;
        }

		// convert 64bit encoded jwt
		// token to json object 
		function base64_decode(str) {
        	var output = str.replace('-', '+').replace('_', '/');
	        switch (output.length % 4) {
	            case 0:
	                break;
	            case 2:
	                output += '==';
	                break;
	            case 3:
	                output += '=';
	                break;
	            default:
	                throw 'Illegal base64url string!';
	        }
	        return window.atob(output); //polifyll https://github.com/davidchambers/Base64.js
	    }
	}

})();