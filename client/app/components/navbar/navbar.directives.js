

(function () {

    'use strict';

	angular.module('directives.Navbar', [])
		.directive('alarmsNotification', alarmsNotification)
		.directive('tasksNotification', tasksNotification);

	
	// alarm notification directives
	function alarmsNotification(){
		return {
			restrict: 'A',
			templateUrl: 'components/navbar/alarms.html',
			link: function(scope, element, attrs){
				// scope.$watch('socketData', function(data){
				// 	if(!_.isEmpty(data)){
				// 		// get plate no of current vehicle
			 //            var plateno = scope.tracker.vehicles[data.systemno].plateno;
				// 		angular.forEach(data.alarms, function(alarm){
			 //                toaster.pop("error", plateno , alarm.msg+"!");

			 //                scope.alarms.push({
			 //                    type: alarm.alarm,
			 //                    icon: 'fa fa-bolt',
			 //                    message: alarm.msg,
			 //                    plateno: plateno,
			 //                    time: data.time
			 //                });

			 //                scope.iconWarning[data.systemno] = 'fa-blink';
			 //            });
			 //        }
				// });
			}
		}
	}
	// tasks notification directives
	function tasksNotification(){
		return {
			restrict: 'A',
			templateUrl: 'components/navbar/notifications.html',
			link: function(scope, element, attrs){

	   //          scope.$watch('socketData', function(data){
				// 	if(!_.isEmpty(data) && _.has(data, 'command')){
				// 		// get plate no of current vehicle
			 //            var plateno = scope.tracker.vehicles[data.systemno].plateno;
				// 		toaster.pop("success", plateno , "Update command successful!");
			 //            scope.notifications.push({
			 //                icon: 'fa fa-refresh',
			 //                message: 'Update command Successful',
			 //                plateno: plateno,
			 //                time: data.time
			 //            });
			 //        }
				// });
			}
		}
	}

})();