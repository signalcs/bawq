
(function(){

	'use strict';

	angular
		.module('services.Login')
		.factory('TokenInterceptor', TokenInterceptor);

	TokenInterceptor.$inject = ['$q', '$window', '$location', 'AuthenticationService'];
	function TokenInterceptor($q, $window, $location, AuthenticationService) {
	    return {
	        request: function (config) {
	            config.headers = config.headers || {};
	            if ($window.localStorage.token) {
	            	config.headers.Authorization = 'Bearer ' + $window.localStorage.token;
	            }
	            return config;
	        },
	        requestError: function(rejection) {
	            return $q.reject(rejection);
	        },
	        response: function (response) {
	            if (response != null && response.status == 200 && $window.localStorage.token && !AuthenticationService.isLogin) {
	                AuthenticationService.isLogin = true;
	            }
	            return response || $q.when(response);
	        },
	        responseError: function(rejection) {
	            if (rejection != null && rejection.status === 401 && ($window.localStorage.token || AuthenticationService.isLogin)) {
	                delete $window.localStorage.token;
	                AuthenticationService.isLogin = false;
	                $location.path("/login");
	            }
	            return $q.reject(rejection);
	        }
	    }
	}

})();