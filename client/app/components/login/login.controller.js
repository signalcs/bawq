(function(){

	'use strict';

	angular
		.module('controller.Login', [])
		.controller('LoginController', LoginController);

	LoginController.$inject = [
		'$scope', 
		'$location', 
		'$window', 
		'AuthenticationService', 
		'Login',
		'toaster'
	];
	function LoginController($scope, $location, $window, AuthenticationService, Login, toaster){

		$scope.login = {};

		$scope.login.signIn = function(){

            if ($scope.login.email != null && $scope.login.password != null) {

            	$scope.login.dataLoading = true;

            	if(!$scope.login.remember) $scope.login.remember = false;

                Login.signIn($scope.login.email, $scope.login.password, $scope.login.remember).success(function(data) {

                	$scope.login.dataLoading = false;
                    AuthenticationService.isLogin = true;
                    // save to localstorage tokens data and user info
                    $window.localStorage.token = data.token;
			        // redirect to tracker
                    $location.path("/tracker");

                }).error(function(status, data) {

                	$scope.login.dataLoading = false;
                    toaster.pop("error", "Unauthorized", status.message);
                    
                });
            }
        }

	}

})();
