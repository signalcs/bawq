
(function(){

	'use strict';

	angular
		.module('services.Login')
		.factory('AuthenticationService', AuthenticationService);

	AuthenticationService.$inject = ['$window'];
	function AuthenticationService($window) { 
		var token;
		var user = {};

		if($window.localStorage.token){
			token = $window.localStorage.token.split('.')[1];
        	user = JSON.parse(url_base64_decode(token));
		}
		
        //this is used to parse the profile
	    function url_base64_decode(str) {
	        var output = str.replace('-', '+').replace('_', '/');
	        switch (output.length % 4) {
	            case 0:
	                break;
	            case 2:
	                output += '==';
	                break;
	            case 3:
	                output += '=';
	                break;
	            default:
	                throw 'Illegal base64url string!';
	        }
	        return window.atob(output); //polifyll https://github.com/davidchambers/Base64.js
	    }

	    var auth = {
		    user: user,
	        isLogin: false,
	        check: function() {
		    	if ($window.localStorage.token) {
		        	this.isLogin = true;
		      	} else {
		        	this.isLogin = false;
		      	}
		    }
	    }
	    return auth;
	}

})();