(function(){

	'use strict';

	angular
		.module('services.Login', [])
		.factory('Login', Login);

	Login.$inject = ['$http', '$location', '$window', 'AuthenticationService', 'ENV'];
	function Login($http, $location, $window, AuthenticationService, ENV){
		return {
			signIn: function(username, password, remember) {
	            return $http.post(ENV.api_url + '/api/login', { auth : window.btoa(username+":"+password+":"+remember) });
	        },
	        logOut: function() {
      			if (AuthenticationService.isLogin) {

			        AuthenticationService.isLogin = false;
			        delete $window.localStorage.token;

			        $location.path("/login");
      			}
	        },
		}
	}

})();
