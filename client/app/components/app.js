
(function () {

'use strict';

    /**
     * @ngdoc overview
     * @name trackerAppApp
     * @description
     * # trackerAppApp
     *
     * Main module of the application.
     */
    angular
    .module('trackerApp', [
        'config',
        'toaster',
        'ngAnimate',
        'ngRoute',
        'ngTouch',
        'ngMap',
        'controller.Login',
        'services.Login',
        'controller.Sidebar',
        'directives.Sidebar',
        'controller.Header',
        'directives.Header',
        'controller.Tracker',
        'directives.Tracker',
        'services.Tracker',
        'filters.Tracker',
        'directives.Navbar',
        'controller.Report',
        'services.Report',
        'directives.Report',
        'controller.Settings',
        'directives.Settings',
        'services.Settings',
        'services.socketIO',
        'reverseGeocode',
        'angular-timezone-selector'
    ])
    .constant('_', window._)
    .config(config)
    .run(run);

    config.$inject = ['$routeProvider', '$httpProvider', '$locationProvider'];
    function config($routeProvider, $httpProvider, $locationProvider) {
        $routeProvider
            .when('/login', {
                controller: 'LoginController',
                templateUrl: 'components/login/login.html',
                title: 'Login Page',
                bodyclass: 'login-page'
            })
            .when('/tracker', {
                controller: 'TrackerController',
                templateUrl: 'components/tracker/tracker.html',
                title: 'Vehicle Tracker',
                bodyclass: 'skin-blue sidebar-mini fixed'
            })
            .when('/reports', {
                controller: 'ReportController',
                templateUrl: 'components/report/report.html',
                title: 'Reports',
                bodyclass: 'skin-blue sidebar-mini fixed'
            })
            .otherwise({
                redirectTo: '/login'
            });

        $locationProvider.html5Mode(true);
        $httpProvider.interceptors.push('TokenInterceptor');
    }

    run.$inject = ['$rootScope', '$route', '$location', '$window', 'AuthenticationService'];
    function run($rootScope, $route, $location, $window, AuthenticationService){
        $rootScope.$on("$routeChangeStart", function(event, nextRoute, currentRoute) {
            // check if authenticated when refreshed
            // or when redirected to other page
            AuthenticationService.check();
            //redirect only if both isAuthenticated is false and no token is set
            if (!AuthenticationService.isLogin && !$window.localStorage.token) {
                $location.path("/login");
            }
        });

        $rootScope.$on("$routeChangeSuccess", function(currentRoute, previousRoute){
            //Change page title, based on Route information
            $rootScope.title = $route.current.title;
            $rootScope.bodyclass = $route.current.bodyclass;
        });

        moment.locale('en', {
            calendar : {
                lastDay : '[Yesterday at] LT',
                sameDay : '[Today at] LT',
                nextDay : '[Tomorrow at] LT',
                lastWeek : 'DD/MM/YYYY LT',
                nextWeek : 'DD/MM/YYYY LT',
                sameElse: 'DD/MM/YYYY LT'
            }
        });
    }

})();

    
