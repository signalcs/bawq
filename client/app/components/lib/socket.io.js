(function(){

    'use strict';

    angular
        .module('services.socketIO', [])
        .factory('socketIO', socket);

    socket.$inject = ['$rootScope', 'ENV', '$window'];
    function socket($rootScope, ENV, $window) {
        var socket = false;

        function getSocket(socket_ins){
            socket = socket_ins;
        }

        return {
            io: function(){
                return socket;
            },
            disconnect: function(){
                socket.disconnect();
            },
            connect: function(){
                socket = io.connect(ENV.websocket_url, {'query': 'token=' + $window.localStorage.token, 'force new connection': true, 'reconnect': true});
                getSocket(socket);
            },
            on: function (eventName, callback) {
                socket.on(eventName, function () {  
                    var args = arguments;
                    $rootScope.$apply(function () {
                        callback.apply(socket, args);
                    });
                });
            },
            emit: function (eventName, data, callback) {
                socket.emit(eventName, data, function () {
                    var args = arguments;
                    $rootScope.$apply(function () {
                        if (callback) {
                            callback.apply(socket, args);
                        }
                    });
                })
            }
        }
    }

})();