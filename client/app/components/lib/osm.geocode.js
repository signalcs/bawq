
(function(){
    
    'use strict';

    angular.module('reverseGeocode', []).factory('Geocode', function ($http, $window, $q, $timeout) {
        var locations = $window.localStorage.locations ? JSON.parse($window.localStorage.locations) : {};
     
        var queue = [];
     
        // Amount of time (in milliseconds) to pause between each trip to the
        // Geocoding API, which places limits on frequency.
        var queryPause = 0;
     
        /**
        * executeNext() - execute the next function in the queue.
        * If a result is returned, fulfill the promise.
        * If we get an error, reject the promise (with message).
        * If we receive OVER_QUERY_LIMIT, increase interval and try again.
        */
        var executeNext = function () {
            var task = queue[0];

            $http({
                method : "GET",
                url : "https://ewatrack.com/nominatim/reverse",
                params: {
                    format: 'json',
                    lat: task.latlng.lat,
                    lon: task.latlng.lng,
                    zoom: 16,
                    addressdetails: 1,
                    namedetails: 1
                }
            }).then(function(response){

                if(response.status == 200){
                    var geoaddress = {
                        address: response.data.display_name
                    };

                    queue.shift();

                    locations[task.latlng.lat, task.latlng.lng] = geoaddress;
                    $window.localStorage.locations = JSON.stringify(locations);

                    task.d.resolve(geoaddress);

                    if (queue.length) {
                        $timeout(executeNext, queryPause);
                    }
                }

            });
        };
     
        return {
            addressForLatLng : function (latlng) {
                var d = $q.defer();

                if ([latlng.lat, latlng.lng] in locations) {
                    $timeout(function () {
                        d.resolve(locations[latlng.lat, latlng.lng]);
                    });
                } else {
                    queue.push({
                        latlng: latlng,
                        d: d
                    });
                    if (queue.length === 1) {
                        executeNext();
                    }
                }
                return d.promise;
            }
        };
    });

})();