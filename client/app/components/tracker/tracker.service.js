(function () {

    'use strict';

    angular
    	.module('services.Tracker', [])
		.factory('Tracker', Tracker);

    Tracker.$inject = ['_', '$q', '$timeout', 'Geocode','$http', 'ENV'];
	function Tracker(_, $q, $timeout, Geocode, $http, ENV) {
        var tracker = this;

        function preload(image) {
            var deffered = $q.defer();

            if (image.complete) {
                deffered.resolve(image);
            } else {
                image.addEventListener('load', function() {
                    deffered.resolve(image);
                });
                image.addEventListener('error', function() {
                    deffered.reject(image);
                });
            }
            return deffered.promise;
        }

        function createMarker(device, callback){
            var markerIcon = new Image();
            markerIcon.src = '/assets/images/disconnected.png';

            if(device.devicestatus == 1){
                markerIcon.src = '/assets/images/running.png';
            }else if(device.devicestatus == 2){
                markerIcon.src = '/assets/images/idle.png';
            }else if(device.devicestatus == 3){
                markerIcon.src = '/assets/images/connected.png';
            }else if(device.devicestatus == 4){
                markerIcon.src = '/assets/images/nosignal.png';
            }

            preload(markerIcon).then(function(img){
                var canvas = document.createElement('canvas');
                canvas.width  = img.width  ;
                canvas.height = img.height ;

                var context = canvas.getContext('2d') ;
                context.save();
                context.translate( img.width / 2, img.height / 2) ;
                context.rotate(device.orientation*Math.PI/180);
                context.drawImage ( img, -img.width / 2, -img.height / 2) ; 
                context.restore() ;

                var image = {
                    url: canvas.toDataURL(),
                    size: [38, 38],
                    anchor: [img.width / 2, img.height / 2],
                    scaledSize: [38, 38]
                };

                callback(image);
            });
        }

        return {
            getStatus : function(device, callback){
                $http.get(ENV.api_url + "/api/v1/vehicles/status?systemno="+device).then(function(results) {
                    callback(results.data);
                }); 
            },
            mapDevices : function(devices, locations){
                var deferred = $q.defer();
                var dev = {};
                // get no systemno with no gps signal
                var noGps = _.filter(locations, function(location){
                    return !location.locate;
                }).map(function(location){
                    return location.systemno;
                }).join(',');

                // query location of systemnos without gps signal
                $http.get(ENV.api_url + "/api/v1/vehicles/location?systemno="+noGps)
                    .then(function(results) {
                        var bySystemNo = _.keyBy(results.data, 'systemno');
                        
                        locations.map(function(location){
                            // create canvas icon
                            createMarker(location, function(icon){
                                location.icon = icon;
                            });
                            
                            var duration = moment().diff(moment(location.time), 'minutes');
                            // show offline status when last update is 15mins above
                            if(duration > 15) location.devicestatus = 5;
                            // extend or assign
                            location = _.assign(devices[location.systemno], location);

                            if(!location.locate && bySystemNo[location.systemno]){
                                // var duration = moment().diff(moment(location.time), 'minutes');
                                // // show offline status when last update is 15mins above
                                // if(duration > 15) location.devicestatus = 5;
                                // assign data with last device location
                                location = _.assign(devices[location.systemno], bySystemNo[location.systemno]);
                            }
                            // store it to devices object
                            dev[location.systemno] = location;
                        });

                        deferred.resolve(dev);                   
                    });

                return deferred.promise;
            },
            updateDevice: function (device, callback) {
                createMarker(device, function(icon){
                    device.icon = icon;

                    if(!device.locate){
                        device = _.omit(device, ['location']);
                        callback(device);
                    }else{
                        callback(device);
                    }
                });
            },
            getAddress: function(device, callback){
                // var latlng = new google.maps.LatLng(device.location[1], device.location[0]);
                Geocode.addressForLatLng({'lat': device.location[1], 'lng': device.location[0]}).then(function(result) {
                    if (result) {
                        callback(result.address);
                    }else {
                        callback('Address not located.');
                    }
                });
            }

        }
	}

})();