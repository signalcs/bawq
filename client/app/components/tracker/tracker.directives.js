(function(){

	'use strict';

	angular
		.module('directives.Tracker', [])
		.directive('mapSize', mapSize)
		.directive('tooltip', tooltip)
		.directive('deviceInfo', deviceInfo);

	/**
	 * Map resize directives
	 */
	mapSize.$inject = ['$window'];
	function mapSize($window){
		return {
	        restrict: 'A',
	        link: function(scope, element, attrs) {
	            // change maps height and trigger resize
        		angular.element('#map').height($window.innerHeight - 50);

	            scope.$watch('tracker.map', function(map){
	            	if(map != undefined){
	            		// show loader until tilesloaded event is finished
		                google.maps.event.addListenerOnce(map, 'tilesloaded', function(){
		                	$('.map-wrapper').removeClass('mapLoader');
		                    $("#map").animate({ opacity: 1});
		                });
	            	}
	            });
        		
        		angular.element($window).bind('resize', function(){
	            	angular.element('#map').height($window.innerHeight - 50);
	            });

                google.maps.event.addDomListener($window, "resize", function() {
                    google.maps.event.trigger(scope.tracker.map, "resize");
                });
	        }
	    };
	}

	function tooltip(){
		return {
			restrict: 'A',
			link: function(scope, element, attrs){
				element.tooltip();
			}
		}
	}

	function deviceInfo(){
		return {
			restrict: "E",
			templateUrl: "components/tracker/tracker-info.html",
			replace: true,
			scope: {
				info: "=",
				visible: "=showModal"
			},
			link: function(scope, element, attrs){
				scope.$watch('visible', function(value){
					if(value){
						element.modal('show');
					}else{
						element.modal('hide');
					}
				});

				element.on('hidden.bs.modal', function(){
                    scope.$apply(function(){
						scope.visible = false;
					})
                });
			}
		}
	}

})();
