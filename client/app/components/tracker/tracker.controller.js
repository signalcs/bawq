(function () {

    'use strict';
    
    angular
        .module('controller.Tracker', [])
        .controller('TrackerController', TrackerController);

    TrackerController.$inject = ['_', '$timeout', '$scope', 'Tracker', 'NgMap', 'socketIO', '$http', 'toaster', 'ENV', 'Devices', 'Settings', 'Groups'];

    function TrackerController(_, $timeout, $scope, Tracker, NgMap, socketIO , $http, toaster, ENV, Devices, Settings, Groups){
        // create a tracker 
        // scope namespace
        $scope.tracker = {};
        $scope.tracker.loading = true;

        $scope.tracker.showMarkerInfo = function(systemno){
            var device = $scope.tracker.devices[systemno];
            // pan to device marker
            panMarker(device);
            // get geo address
            Tracker.getAddress(device, function(address){
                device.address = address;
            });
        }

        $scope.tracker.panToMarker = function(){
            var device = $scope.tracker.devices[this.data];
            panMarker(device);
        }

        $scope.tracker.getInfo = function(device){
            // show bootstrap modal box
            $scope.tracker.showInfo = true;
            panMarker(device);
            // get today mileage
            Devices.getTodayMile(device.systemno).then(function(mileage){
                $scope.tracker.devices[device.systemno].todayMile = mileage.mile;
            });
        }

        $scope.tracker.sendUpdate = function(selected){
            var device = $scope.tracker.devices[selected];
            $scope.tracker.loader = true;

            // send config/update to device
            Settings.sendUpdate(device);

            $timeout(function(){
                $scope.tracker.loader = false;
            }, 2000);
        }

        $scope.tracker.setConfig = function(device){
            $scope.tracker.showSetting = true;
            panMarker(device);   
        }

        $scope.tracker.onMapOverlayCompleted = function(e){
            console.log(e.type);
            console.log(e)
        };
        
        $scope.$on('$destroy', function(){
            socketIO.io().removeAllListeners();
        });

        /*================================
        =            Services            =
        ================================*/
        
        Devices.getDevices().then(function(devices){
            // connect to socket io server
            socketIO.connect();
            // get last location from socketIO
            socketIO.on('data:location', function(message){
                var data = JSON.parse(message.data);
                // set trackers devices with last locations
                Tracker.mapDevices(devices, data).then(function(result){
                    $scope.tracker.devices = result;
                    // initialize map after getting devices here
                    // to avoid racing statement with devices request
                    $scope.tracker.map = NgMap.initMap('map');
                    $scope.tracker.loading = false;
                });
                
            });

            // realtime location from socketIO
            socketIO.on('resp:location', function(message){
                var data = JSON.parse(message.data);
                Tracker.updateDevice(data, function(update){
                    if($scope.tracker.devices) _.assign($scope.tracker.devices[data.systemno], update);
                    panToSelected(data);

                    // show toaster on update command
                    if(_.has(data, 'command')){
                        var plateno = $scope.tracker.devices[data.systemno].plateno;
                        toaster.pop("success", plateno, "Update Location Success!");
                    }
                });
            });

        });

        Groups.getGroups().then(function(groups){
            // set trackers groups
            $scope.tracker.groups = groups;
        });
        
        /*=====  End of Services  ======*/

        /*========================================
        =            Helper Functions            =
        ========================================*/
        
        function panMarker(device){
            var latlng = new google.maps.LatLng(device.location[1], device.location[0]);
            $scope.tracker.map.setZoom(17);
            // pan to device location
            $scope.tracker.map.panTo(latlng);
            // set selected device
            $scope.tracker.selected = device.systemno;
            // show and hide sidebar
            $scope.tracker.openSidebar = true;
            // open info window
            $scope.tracker.map.showInfoWindow('vehicleWindow', device.systemno);
        }

        function panToSelected(data){
            if(data.systemno == $scope.tracker.selected && data.locate){
                var device = $scope.tracker.devices[data.systemno];
                var latlng = new google.maps.LatLng(data.location[1], data.location[0]);
                $scope.tracker.map.setCenter(latlng);
                
                Tracker.getAddress(device, function(address){
                    device.address = address;
                });
            }
        }

        /*=====  End of Helper Functions  ======*/

    }

})();
