(function () {

    'use strict';

    angular
    	.module('services.Tracker')
		.factory('Groups', Groups);

    Groups.$inject = ['_', '$http', 'AuthenticationService', 'ENV'];
	function Groups(_, $http, AuthenticationService, ENV) {
        
        return {
            getGroups : function(){
                var companyId = AuthenticationService.user.company_id;

                return $http.get(ENV.api_url + "/api/v1/fleets/"+companyId).then(function(groups){
                    return groups.data
                });
            }
        }
	}

})();