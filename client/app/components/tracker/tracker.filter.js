
(function () {

    'use strict';

	angular.module('filters.Tracker', [])
		.filter('deviceSearch', deviceSearch)
		.filter('accstatus', accstatus)
		.filter('distance', distance)
		.filter('humanized', humanized)
		.filter('time', time)
		.filter('orderObjectBy', orderObjectBy);

	function deviceSearch() {
	  	return function(input, search) {
	  		if (!input) return input;
    		if (!search) return input;
	    	var expected = ('' + search).toLowerCase();
		    var result = {};
		    angular.forEach(input, function(value, key) {
		    	
		      	var actual = ('' + value.plateno).toLowerCase();
		      	if (actual.indexOf(expected) !== -1) {
		        	result[key] = value;
		      	}
		    });
		    return result;
	  	};
	}

	function accstatus() {
	  	return function(input) {
	    	return input == true ? 'ON' : 'OFF';
	  	};
	}

	function distance(){
		var conversionKey = {
       		m: {
           		km: 0.001,
           		m:1
       		},
       		km: {
           		km:1,
           		m: 1000
       		}
   		};
	   	return function (distance, from, to) {
	       	return (distance * conversionKey[from][to]).toFixed(1);
	   	};
	}

	function humanized(){
		return function(time){
			if(time != undefined){
				var localTime  = moment(time);
    			return moment(localTime).fromNow();
			}else{
				return 'No Update';
			}
		}
	}

	function time(){
		return function(time){
			if(time != undefined){
				var localTime  = moment(time);
    			return moment(localTime).calendar();
			}else{
				return 'No Update';
			}
		}
	}

	function orderObjectBy(){
	  	return function(items, field, reverse) {
		    var filtered = [];
		    angular.forEach(items, function(item) {
		      	filtered.push(item);
		    });
		    filtered.sort(function (a, b) {
		      	return (a[field] > b[field] ? 1 : -1);
		    });
		    if(reverse) filtered.reverse();
		    return filtered;

	  	};
	}

})();