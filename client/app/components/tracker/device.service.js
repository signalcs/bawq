(function () {

    'use strict';

    angular
    	.module('services.Tracker')
		.factory('Devices', Devices);

    Devices.$inject = ['_', '$http', 'ENV'];
	function Devices(_, $http, ENV) {
        
        return {
            getDevices : function(){
                return $http.get(ENV.api_url + "/api/v1/vehicles").then(function(devices){
                    return _.keyBy(devices.data, 'systemno');
                });
            },
            getTodayMile : function(systemno){
                return $http.get(ENV.api_url + "/api/v1/vehicles/todaymile?systemno="+systemno).then(function (results) {
                    return results.data;
                });
            }
        }
	}

})();