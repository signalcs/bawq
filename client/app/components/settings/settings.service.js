(function () {

    'use strict';

    angular
    	.module('services.Settings', [])
		.factory('Settings', Settings);

    Settings.$inject = ['socketIO'];
	function Settings(socketIO) {
        var factory = {};

        function sendConfig(device){
            var update = {
                client_id : socketIO.io().id,
                systemno: device.systemno,
                device_type: device.device_type,
                command: device.command,
                data: device.model,
                action: 'at:command'
            };
            return update;
        }

        function updateData(device){
            var update = {
                client_id : socketIO.io().id,
                systemno: device.systemno,
                device_type: device.device_type,
                command: 'com:update',
                action: 'at:command'
            };
            return update;
        }

        return {
            sendConfig : function(device){
                var config = sendConfig(device);
                socketIO.emit('at:command', config);
            },
            sendUpdate : function(device){
                var update = updateData(device);
                socketIO.emit('at:command', update);
            }
        }
	}

})();