(function(){

	'use strict';

	angular
		.module('directives.Settings', ['formly'], function config(formlyConfigProvider){
			formlyConfigProvider.setType([{
	      		name: 'number',
	      		template:'<div class="form-group">'+
	      			'<label>{{options.templateOptions.label}} <span class="required">{{to.sublabel}}</span></label>'+
	      			'<input ng-model="model[options.key]" min={{to.min}} max={{to.max}} class="form-control"/>'+
	      			'</div>'
	    	},
	    	{
	    		name: 'text',
	      		template:'<div class="form-group">'+
	      			'<label>{{options.templateOptions.label}} <span class="required">{{to.sublabel}}</span></label>'+
	      			'<input type="text" ng-model="model[options.key]" class="form-control"/>'+
	      			'</div>'
	    	},
	    	{
	    		name: 'timezone',
	    		template: '<div class="form-group">'+
                            '<label>{{options.templateOptions.label}} <span class="required">{{to.sublabel}}</span></label>'+
                            '<timezone-selector ng-model="model[options.key]" width="100%"></timezone-selector>'+
                        '</div>'
	    	},
	    	{
	    		name: 'select2-single',
	    		template: '<div class="form-group">'+
	                    	'<label>{{to.label}} <span class="required">{{to.sublabel}}</span></label>'+
	                    	'<select ng-model="model[options.key]" class="form-control no-radius" '+
	                    	'style="width: 100%;" ng-options="option[\'id\'] as option[\'text\'] for option in to.options">'+
	                    		'<option value="" selected="selected">Select Options</option>'+
	                	'</select>'+
	            	'</div>'
	    	}]);
		})
		.directive('deviceSettings', deviceSettings);


	function deviceSettings(){
		return {
			restrict: "E",
			templateUrl: 'components/settings/settings-modal.html',
			replace: true,
			scope: {
				visible: "=showModal",
				device: "=",
				configType: "=",
				settingsList: "=",
				formly: "=",
				formConfig: "&"
			},
			link: function(scope, element, attrs){

				scope.$watch('visible', function(value){
					if(value){
						element.modal('show');
					}else{
						element.modal('hide');
					}
				});

				element.on('hidden.bs.modal', function(){
                    scope.$apply(function(){
						scope.visible = false;
					});
                });
			}
		}
	}

})();
