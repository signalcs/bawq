(function () {

    'use strict';
    
    angular
        .module('controller.Settings', [])
        .controller('SettingsController', SettingsController);

    SettingsController.$inject = ['$scope', 'Settings'];
    function SettingsController($scope, Settings){
        $scope.settings = {};

        $scope.settings.formConfig = function(formly, device){ 
            var obj = {
                systemno: device.systemno,
                devicetype: device.device_type,
                command: formly.command,
                model: formly.model
            }
            // send configuration to device
            Settings.sendConfig(obj);
        }

        $scope.settings.engineStatus = [
            { "id": 0, "text": "Disable Engine" },
            { "id": 1, "text": "Enable Engine" }
        ];

        $scope.settings.powerFailure = [
            { "id": 0, "text": "Disable" },
            { "id": 1, "text": "Enable" }
        ];

        $scope.settings.settingsList = {
            M528: [
                { 
                    id: 0, title: 'Set Interval Time',
                    command: 'com:interval', model: {}, options: {},
                    fields: [{
                        key: 'idle', type: 'number',
                        templateOptions: {
                            type: 'number', min: 1, label: 'Device Idle', 
                            sublabel: '*', placeholder: 'interval in Seconds', required: true
                        }
                    },
                    {
                        key: 'moving', type: 'number',
                        templateOptions: {
                            type: 'number', min: 1, label: 'Device Moving', 
                            sublabel: '*', placeholder: 'interval in Seconds', required: true
                        }
                    }]
                },
                { 
                    id: 1, title: 'Set Timezone',
                    command: 'com:timezone', model: {}, options: {},
                    fields: [{
                        key: 'timezone', type: 'timezone',
                        templateOptions: {
                            type: 'timezone', label: 'Timezone', sublabel: '*',
                            placeholder: '+0300', required: true
                        }
                    }]
                },
                { 
                    id: 2, title: 'Engine Control',
                    command: 'com:engine', model: {}, options: {},
                    fields: [{
                        key: 'status', type: 'select2-single',
                        templateOptions: {
                            label: 'Select Engine Status', sublabel: '*',
                            options: $scope.settings.engineStatus, required: true
                        }
                    }]
                },
                { 
                    id: 3, title: 'Set Speed Alarm',
                    command: 'com:speed', model: {}, options: {},
                    fields: [{
                        key: 'limit', type: 'number',
                        templateOptions: {
                            type: 'number', min: 0, max: 255, label: 'Set Speed Limit', 
                            sublabel: '*', placeholder: '0 - 255 (Km/h)', required: true
                        }
                    }]
                },
                { 
                    id: 4, title: 'Power Failure Alarm',
                    command: 'com:power', model: {}, options: {},
                    fields: [{
                        key: 'power', type: 'select2-single',
                        templateOptions: {
                            label: 'Select Status', sublabel: '*',
                            options: $scope.settings.powerFailure, required: true
                        }
                    }]
                },
                { 
                    id: 5, title: 'Set Parking Limit'
                },
                { 
                    id: 6, title: 'Peripheral Settings'
                }
            ],

            GV300: [
                { 
                    id: 0, title: 'Set Interval Time'
                },
                { 
                    id: 1, title: 'Set Timezone'
                },
                { 
                    id: 2, title: 'Engine Control',
                    command: 'com:engine', model: {}, options: {},
                    fields: [{
                        key: 'status', type: 'select2-single',
                        templateOptions: {
                            label: 'Select Engine Status', sublabel: '*',
                            options: $scope.engineStatus, required: true
                        }
                    }]
                },
                { 
                    id: 3, title: 'Set Speed Alarm',
                    command: 'com:speed',model: {}, options: {},
                    fields: [{
                        key: 'mode', type: 'select2-single',
                        templateOptions: {
                            label: 'Select Mode',sublabel: '*',
                            options: $scope.speedMode,required: true
                        }
                    },
                    {
                        key: 'minspeed',type: 'number',
                        templateOptions: {
                            type: 'number', min: 0, max: 400, label: 'Min Speed', 
                            sublabel: '*', placeholder: '0 - 400 (Km/h)', required: true
                        }
                    },
                    {
                        key: 'maxspeed', type: 'number',
                        templateOptions: {
                            type: 'number', min: 0, max: 400, label: 'Max Speed',
                            sublabel: '*', placeholder: '0 - 400 (Km/h)', required: true
                        }
                    }]
                },
                { 
                    id: 4, title: 'Set Idling Alarm'
                },
                { 
                    id: 5, title: 'Set Tow Alarm'
                }
            ]
        };

    }

})();