(function () {

    'use strict';
    
    angular
        .module('controller.Report', ['datatables', 'datatables.bootstrap'])
        .controller('ReportController', ReportController);

    ReportController.$inject = ['$scope', '$http', 'NgMap', 'ReportService', 'Devices', 'Tracker', 'Geocode', 'ENV', '$timeout', '$interval'];
    function ReportController($scope, $http, NgMap, ReportService, Devices, Tracker, Geocode, ENV, $timeout, $interval){
        $scope.reports = {};

        $scope.reports.reportSelect = function(type){
            if(type == 'trip'){
                $scope.reports.reportType = 'Trip Report';
            }else if(type == 'history'){
                $scope.reports.loading = true;
                // initialize map on history reports
                $timeout(function(){
                    $scope.reports.loading = false;
                    $scope.reports.type = type;
                    $scope.reports.map = NgMap.initMap('reportsMap');   
                });
            }
        }

        $scope.reports.playback = function(){
            $scope.reports.path = [];
            $scope.reports.bounds = new google.maps.LatLngBounds();
            var from = $scope.reports.reportFrom;
            var to = $scope.reports.reportTo;
            var systemno = $scope.reports.selectedDevice.systemno;

            var data = { 
                systemno: systemno,
                start: moment(from).toISOString(),
                end: moment(to).toISOString()
            };

            playback(data);
        }

        $scope.reports.showInfo = function(data){
            var latlng = new google.maps.LatLng(this.data.location[1], this.data.location[0]);
            $scope.reports.infoData = this.data;
            // open info window
            $scope.reports.map.showInfoWindow('vehicleWindow', this.data._id);
            // pan to map
            $scope.reports.map.panTo(latlng);
            $scope.reports.map.setZoom(15);
        }

        $scope.reports.showInfoTable = function(data){
            var latlng = new google.maps.LatLng(data.location[1], data.location[0]);
            $scope.reports.infoData = data;
            // open info window
            $scope.reports.map.showInfoWindow('vehicleWindow', data._id);
            // pan to map
            $scope.reports.map.panTo(latlng);
            $scope.reports.map.setZoom(18);
        }

        /*================================
        =            Services            =
        ================================*/
        
        Devices.getDevices().then(function(devices){
            $scope.reports.devices = devices;
        });
        
        /*=====  End of Services  ======*/

        /*========================================
        =            Helper functions            =
        ========================================*/

        function addMarker(result, data){
            var map = $scope.reports.map;
            var bounds = $scope.reports.bounds;
            $scope.reports.history = [];
            $scope.reports.totalDisplayed = 10;

            result.map(function(location, key){
                var localTime  = moment(location.time);
                location.time = moment(localTime).calendar();

                location.mile = (location.mile * 0.001).toFixed(1);
                location.accstatus = location.accstatus == true ? 'ON' : 'OFF';

                location.icon = 'https://maps.gstatic.com/intl/en_ALL/mapfiles/markers2/measle.png';
                $scope.reports.history.push(location);
                $scope.reports.path.push([location.location[1], location.location[0]]);
                var loc = new google.maps.LatLng(location.location[1], location.location[0]);

                bounds.extend(loc);
            });

            if(result.length > 0){
                map.setCenter(bounds.getCenter());
                map.fitBounds(bounds);
            }

            $scope.reports.generating = false;

            if(result.length > 5){
                var times = result.length / 5;
                
                for (var i = 0; i < times; i++) {
                    $timeout(function(){
                        $scope.reports.totalDisplayed += 5;
                    }, 500 * i);
                };
            }

            
        }

        function playback(data){
            $scope.reports.generating = true;

            ReportService.historyReport(data).then(function(result){
                addMarker(result, data);
            });
        }

    	// $scope.reports = false;
     //    $scope.loading = false;
     //    $scope.tripReport = false;
     //    $scope.reportsCtrl.generating = false;

     //    $scope.dtOptions = DTOptionsBuilder.newOptions()
     //    .withPaginationType('full_numbers')
     //    .withDOM('Bfrtip')
     //    .withButtons([
     //        'copyHtml5',
     //        'excelHtml5',
     //        'csvHtml5',
     //        'pdfHtml5'
     //    ]);
        
     //    $scope.reportsCtrl.generateReport = function(obj){
     //    	$scope.reports = true;
     //    	$scope.loading = true;
     //        $scope[obj.type](obj); // will run each report types
     //    }

     //    $scope.tripreport = function(obj){
     //    	$scope.reportsCtrl.generating = true;
     //    	$scope.tripReport = true;

     //        var from = moment(obj.from).toISOString();
     //        var to = moment(obj.to).toISOString();            
     //    	// ReportService.tripReport(obj);

     //    	$http.get(ENV.api_url + "/api/v1/trip-report?systemno="+obj.systemno+"&from="+from+"&to="+to).then(function(data){
     //    		$scope.reportsCtrl.generating = false;
     //            $scope.loading = false;
     //            $scope.tripReport = data.data;

     //            console.log(data.data)

     //            $scope.$parent.$broadcast('closeSidebar');

     //            angular.forEach($scope.tripReport, function(rep){

     //                rep.plateno = $scope.tracker.vehicles[rep.systemno].plateno;

     //                angular.forEach(rep.result, function(result){

     //                    if(result.startAddress[0] != undefined || result.startAddress[1] != undefined){
     //                        var startlatlng = new google.maps.LatLng(result.startAddress[1], result.startAddress[0]);

     //                        Geocode.addressForLatLng(startlatlng)
     //                        .then(function(data){
     //                            result.start = data.address;
     //                        })
     //                        .catch(function(err){
     //                            console.log(err)
     //                        });
     //                    }

     //                    if(result.destAddress[0] != undefined || result.destAddress[1] != undefined){
     //                        var destlatlng = new google.maps.LatLng(result.destAddress[1], result.destAddress[0]);

     //                        Geocode.addressForLatLng(destlatlng)
     //                        .then(function(data){
     //                            result.dest = data.address;
     //                        })
     //                        .catch(function(err){
     //                            console.log(err)
     //                        });
     //                    }
                        

     //                });
                   
     //            });
     //        });
     //    }

    }

})();