(function(){

	'use strict';

	angular
		.module('directives.Report', [])
		.directive('tripReport',tripReport)
		.directive('historyPlayback',historyPlayback);

	function tripReport(){
		return {
			restrict: 'E',
			templateUrl: 'components/report/report-trip.html'
		}
	}

	function historyPlayback($window, $timeout, DTOptionsBuilder, DTColumnDefBuilder ){
		return {
			restrict: 'E',
			templateUrl: 'components/report/report-history.html',
			link: function(scope, element, attrs){

				scope.$watch('reports.type', function(value){
					if(value == 'history'){
						angular.element('#reportsMap').height($window.innerHeight * 0.55);
						angular.element('.table-history').height($window.innerHeight * 0.45 - 50);

						$('.map-wrapper').removeClass('mapLoader');
	                    $("#reportsMap").animate({ opacity: 1});

						var scrollY = angular.element('.table-history').height() - 50;
							
						scope.reports.history = [];

						scope.reports.dtOptions = DTOptionsBuilder.newOptions()
		                .withDOM('rt')
		                .withOption('deferRender', true)
		                .withOption('scrollY', scrollY+'px')
		                .withOption('scrollCollapse', true)
		                .withOption('paging', false)
		                .withOption('responsive', true)
		                .withBootstrap();

		                scope.reports.dtColumnDefs = [
		                   	DTColumnDefBuilder.newColumnDef(0).notSortable().withClass('text-center').withOption('type', 'date'),
		                    DTColumnDefBuilder.newColumnDef(1).notSortable().withOption('width', '50%'),
		                    DTColumnDefBuilder.newColumnDef(2).notSortable().withClass('text-center'),
		                    DTColumnDefBuilder.newColumnDef(3).notSortable().withClass('text-center'),
		                    DTColumnDefBuilder.newColumnDef(4).notSortable().withClass('text-center'),
		                    DTColumnDefBuilder.newColumnDef(5).notSortable().withClass('text-center')
		                ];

		                scope.reports.dtInstance = {};
					}
				});

				scope.$watch('reports.generating', function(value){
					if(value){
						$('.map-wrapper').addClass('mapLoader');
	                    $("#reportsMap").animate({ opacity: 0});
					}else{
						$('.map-wrapper').removeClass('mapLoader');
	                    $("#reportsMap").animate({ opacity: 1});
					}
				});
			}
		}
	}

})();
