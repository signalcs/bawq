(function () {

    'use strict';

    angular
    	.module('services.Report', [])
		.factory('ReportService', ReportService);

    ReportService.$inject = ['$http', '$q',  'ENV'];
	function ReportService($http, $q, ENV) {
        var queue=[];

        function execNext() {
            var task = queue[0];

            $http({method: 'POST', url: ENV.api_url + '/api/v1/playback', data: task.data}).then(function(result) {
                queue.shift();
                task.d.resolve(result.data);
                if (queue.length>0) execNext();
            }, function(err) {
                task.d.reject(err);
            });
        }

        return {

            tripReport: function(obj){
                $http.get(ENV.api_url + "/api/v1/trip-report?systemno="+obj.systemno+"&from="+obj.from+"&to="+obj.to).then(function(data){
                    
                });  
            },
            historyReport: function(data){
                var d = $q.defer();
                
                queue.push({data:data, d:d});
                if (queue.length===1) execNext();            
                return d.promise;
            }
        }
	}

})();