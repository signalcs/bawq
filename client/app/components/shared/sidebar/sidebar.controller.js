(function () {

    'use strict';
    
    angular
        .module('controller.Sidebar', [])
        .controller('SidebarController', SidebarController);

    SidebarController.$inject = ['$scope'];

    function SidebarController($scope){
    	$scope.sidebar = {};

    	$scope.sidebar.showLegend = function(){
    		$scope.sidebar.visible = true;
    	}
    }

})();