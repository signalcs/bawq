(function(){

	'use strict';

	angular
		.module('directives.Sidebar', [])
		.directive('sidebarDevices', sidebarDevices)
		.directive('sidebarGroups', sidebarGroups)
		.directive('modalLegend', modalLegend)
		.directive('daterangeBtn', daterangeBtn);

	function sidebarDevices(){
		return {
       		restrict: 'E',
       		templateUrl: 'components/shared/sidebar/sidebar-device.html',
       		replace: true,
	       	scope: {
	           device: "=",
	           selected: "=setActive",
	           getInfo: "&",
	           setConfig: "&",
	           showMarkerInfo: "&"
	       	}
   		}
	}

	function sidebarGroups(){
		return {
       		restrict: 'E',
       		templateUrl: 'components/shared/sidebar/sidebar-group.html',
       		replace: true,
	       	scope: {
	           group: "="
	       	}
   		}
	}

	function modalLegend(){
		return {
			restrict: "E",
			templateUrl: 'components/shared/sidebar/modal-legend.html',
			replace: true,
			scope: {
				visible: "=showModal"
			},
			link: function(scope, element, attrs){
				scope.$watch('visible', function(value){
					if(value){
						element.modal('show');
					}else{
						element.modal('hide');
					}
				});

				element.on('hidden.bs.modal', function(){
                    scope.$apply(function(){
						scope.visible = false;
					})
                });
			}
		}
	}

	// bootstrap based daterange button
	function daterangeBtn(){
    	return{
    		link: function(scope, element, attrs){
    			scope.reports.reportFrom = moment().startOf('day').format('YYYY-MM-DD HH:mm');
	        	scope.reports.reportTo = moment().endOf('day').format('YYYY-MM-DD HH:mm');

    			$(element).daterangepicker({
		            ranges: {
				      	'Today': [moment().startOf('day'), moment().endOf('day')],
				      	'Yesterday': [moment().subtract(1, 'days').startOf('day'), moment().subtract(1, 'days').endOf('day')]
				    },
				    startDate: moment().startOf('day'),
				    endDate: moment().endOf('day'),
				    maxDate: moment().endOf('day'),
				    timePicker: true,
		            timePickerIncrement: 1,
		            timePickerSeconds: true,
		            timePicker24Hour: true,
		            format: 'YYYY-MM-DD HH:mm'
		        },
			        function (start, end) {
			        	scope.$apply(function() {
			        		scope.reports.reportFrom = start.format('YYYY-MM-DD HH:mm');
			        		scope.reports.reportTo = end.format('YYYY-MM-DD HH:mm');
			        	});
			        }
        		);

    		}
    	}
    }

})();