(function () {

    'use strict';
    
    angular
        .module('controller.Header', [])
        .controller('HeaderController', HeaderController);

    HeaderController.$inject = ['$scope', 'Login', 'socketIO'];

    function HeaderController($scope, Login, socketIO){

    	$scope.header.logOut = function logOut() {
            // UserAuthenticationService
            Login.logOut();
            // destroy socket.io connection
            socketIO.disconnect();
        }
    }

})();