	
(function(){

	'use strict';

	angular.module('directives.Header', [])
		.directive('adminLteSidebarToggle', adminLteSidebarToggleDirective);

	function adminLteSidebarToggleDirective() {
		return {
			restrict: 'E',
			link: link,
			template: '<a href="" class="navbar-toggle sidebar-toggle" data-toggle="offcanvas" role="button">'+
            	'<span class="sr-only">Toggle navigation</span>'+
            	'<span class="icon-bar"></span>'+
            	'<span class="icon-bar"></span>'+
            	'<span class="icon-bar"></span>'+
        		'</a>',
			scope: {
				selected: '=openSidebar'
			}
		};

		function link($scope, $element, $attrs) {
			//Get the screen sizes
			var screenSizes = {
				xs: 480,
				sm: 768,
				md: 992,
				lg: 1200
			};

			$scope.$watch('selected', function(value, oldValue){
				if(value){
					if (window.innerWidth < (screenSizes.sm - 1)) {
						var body = angular.element('body');
						body.removeClass('sidebar-open');
						body.removeClass('sidebar-collapse');
					}
					$scope.selected = false;
				}
			});

			//Enable sidebar toggle
			$element.click(function (e) {
				e.preventDefault();
				//Enable sidebar push menu
				if (window.innerWidth < (screenSizes.sm - 1)) {
					var body = angular.element('body');

					if(body.hasClass('sidebar-open')){
						body.removeClass('sidebar-open');
						body.removeClass('sidebar-collapse');
					}else{
						body.addClass('sidebar-open')
					}
				}
			});
		}
	}

})();
